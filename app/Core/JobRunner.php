<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 24-1-2017
 * Time: 16:13
 */

namespace App\Core;


use Jobby\Jobby;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TaskRunner
 * @package App\Core
 *
 * Command om alle jobs te draaien in het systeem
 *
 * Nieuwe jobs kunnen onder configuration/jobs.php toegevoegd worden
 */
class JobRunner extends Command{

    protected $jobby;

    public function __construct(Jobby $jobby){
        parent::__construct();
        $this->jobby = $jobby;
    }

    public function configure(){
        $this->setName('tasks:run')
            ->setDescription('Run tasks');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $this->jobby->run();
    }
}