<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 14:02
 */

namespace App\Core;

use \Illuminate\Events\Dispatcher as EventDispatcher;
use Interop\Container\ContainerInterface;

class Dispatcher extends EventDispatcher{

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    public function createClassCallable($listener){
        list($class, $method) = $this->parseClassCallable($listener);

        if ($this->handlerShouldBeQueued($class)) {
            return $this->createQueuedHandlerCallable($class, $method);
        } else {
            return [$this->container->get($class), $method];
        }
    }

}