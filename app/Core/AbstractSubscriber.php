<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 11:52
 */

namespace App\Core;


use Interop\Container\ContainerInterface;

abstract class AbstractSubscriber{

    protected $container;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    abstract public function subscribe(Dispatcher $dispatcher);
}