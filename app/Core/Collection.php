<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Core;

/**
 * Description of Collection
 *
 * @author thomas
 *
 * Geeft een Collection class helper waarmee gemakkelijker met arrays gewerkt kan worden.
 */
class Collection {

    public $data = array();

    public function set($element) {
        $this->data[] = $element;
    }

    public function count() {
        return count($this->data);
    }

    public function getData() {
        return $this->data;
    }

    public function slice($callback) {
        $collection = new Collection();

        foreach ($this->data as $model) {
            if ($callback($model)) {
                $collection->set($model);
            }
        }


        return $collection;
    }

}
