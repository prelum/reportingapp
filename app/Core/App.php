<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 14-1-17
 * Time: 15:57
 */

namespace App\Core;

use DI\ContainerBuilder;
use DI\Container;
use Dotenv\Dotenv;
use Interop\Container\ContainerInterface;
use Invoker\Invoker;
use Invoker\ParameterResolver\AssociativeArrayResolver;
use Invoker\ParameterResolver\Container\TypeHintContainerResolver;
use Invoker\ParameterResolver\DefaultValueResolver;
use Invoker\ParameterResolver\ResolverChain;
use Slim\Handlers\Error;
use Slim\Handlers\NotAllowed;
use Slim\Handlers\NotFound;
use Slim\Handlers\PhpError;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;

class App extends \Slim\App
{
    /**
     * App constructor.
     * @param array $container
     *
     * Laden van de de environment gegevens, de algemene services voor de router en de andere services geconfigureerd onder configuration/services.php
     */
    public function __construct($container = [])
    {
        $dotenv = new Dotenv(__DIR__ . '/../../');
        $dotenv->load();
        $builder = new ContainerBuilder;
        $settings = require __DIR__ . '/../../configuration/settings.php';
        $builder->addDefinitions(['settings' => $settings]);
        $builder->addDefinitions([
            // Default Slim services
            'router' => function (ContainerInterface $c) {
                return (new Router())->setCacheFile($c->get('settings')['routerCacheFile']);
            },
            Router::class => \DI\get('router'),
            'errorHandler' => function (ContainerInterface $c) {
                return new Error($c->get('settings')['displayErrorDetails']);
            },
            'phpErrorHandler' => function (ContainerInterface $c) {
                return new PhpError($c->get('settings')['displayErrorDetails']);
            },
            'notFoundHandler' => \DI\object(NotFound::class),
            'notAllowedHandler' => \DI\object(NotAllowed::class),
            'environment' => function () {
                return new Environment($_SERVER);
            },
            'request' => function (ContainerInterface $c) {
                return Request::createFromEnvironment($c->get('environment'));
            },
            'response' => function (ContainerInterface $c) {
                $headers = new Headers(['Content-Type' => 'text/html; charset=UTF-8']);
                $response = new Response(200, $headers);
                return $response->withProtocolVersion($c->get('settings')['httpVersion']);
            },
            'foundHandler' => \DI\object(ControllerInvoker::class)
                ->constructor(\DI\get('foundHandler.invoker')),
            'foundHandler.invoker' => function (ContainerInterface $c) {
                $resolvers = [
                    // Inject parameters by name first
                    new AssociativeArrayResolver,
                    // Then inject services by type-hints for those that weren't resolved
                    new TypeHintContainerResolver($c),
                    // Then fall back on parameters default values for optional route parameters
                    new DefaultValueResolver(),
                ];
                return new Invoker(new ResolverChain($resolvers), $c);
            },
            'callableResolver' => \DI\object(CallableResolver::class),
            ContainerInterface::class => \DI\get(Container::class),
        ]);
        $builder->addDefinitions(require __DIR__ . '/../../configuration/services.php');
        $container = $builder->build();
        parent::__construct($container);
    }
}