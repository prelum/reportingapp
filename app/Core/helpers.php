<?php

/**
 * Helper-functies die globaal gebruikt kunnen worden
 */


/**
 * @param $data
 *
 * Dumpt data en stopt executie van de applicatie
 */
function dump($data){
    var_dump($data);
    exit;
}

/**
 * @param array $array
 * @return array
 *
 * Geeft een array terug waarin voor elke key in een 2-dimensionale array de gemiddelde waarde is berekend
 */
function keyedArraySumAndAverage(array $array){
    $merged = [];
    $counter = [];
    foreach($array as $set){
        foreach($set as $key => $value){
            (key_exists($key, $merged)) ? $merged[$key] = $merged[$key] + $value : $merged[$key] = $value;
            (key_exists($key, $counter)) ? $counter[$key] = $counter[$key] + 1 : $counter[$key] = 1;
        }
    }

    $results = [];
    foreach($merged as $status => $value){
        $results[$status] = $value / $counter[$status];
    }
    return $results;
}

