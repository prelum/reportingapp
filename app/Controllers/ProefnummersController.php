<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\SubscriptionRepository;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 */
class ProefnummersController extends Controller {

    private $view;
    private $subscriptionRepository;

    public function __construct(Twig $view, SubscriptionRepository $subscriptionRepository) {
        $this->view = $view;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function indexAction(Response $response, $productCode) {
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $subscriptions = $this->subscriptionRepository->getSubscriptions($product, "AND abonnementType = 'proefnummer'");

        $converted = $subscriptions->slice(function($subscription) {
            return !$subscription->isRegistrationTypeSubscriptionType();
        });

        $data = array(
            'product' => $product,
            'subscriptions' => $subscriptions,
            'converted' => $converted,
            'conversion_ratio' => ($converted->count() == 0 || $subscriptions->count() ==0)? 0 : $converted->count() / $subscriptions->count() * 100
        );
        $this->view->render($response, 'proefnummers.html.twig', $data);
    }

}
