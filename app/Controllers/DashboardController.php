<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\ElearningRepository;
use App\Repositories\GoogleAnalyticsRepository;
use App\Repositories\KPIRepository;
use App\Services\DatetimeDispatcher;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 *
 *
 */
class DashboardController extends Controller{

    protected $view;
    protected $dateTimeDispatcher;
    protected $elearningRepository;
    protected $googleRepository;
    protected $kpi;

    public function __construct(Twig $view, DatetimeDispatcher $datetimeDispatcher, ElearningRepository $elearningRepository, GoogleAnalyticsRepository $googleAnalyticsRepository, KPIRepository $KPIRepository){
        $this->dateTimeDispatcher = $datetimeDispatcher;
        $this->elearningRepository = $elearningRepository;
        $this->googleRepository = $googleAnalyticsRepository;
        $this->kpi = $KPIRepository;
        $this->view = $view;
    }

    public function indexAction(Response $response, $productCode){
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $this->kpi->setProduct($product);
        $data = array(
            'datums' => array(
                'vorig_jaar' => $this->dateTimeDispatcher->beginVorigJaar()->format('Y'),
                'dit_jaar' => $this->dateTimeDispatcher->beginDitJaar()->format('Y'),
                'datum_dit_jaar' => $this->dateTimeDispatcher->nu()->format('Y-m-d'),
                'datum_vorig_jaar' => $this->dateTimeDispatcher->vorigJaar()->format('Y-m-d'),
            ),
            'product' => $product,
            'data' => array(
                'toetsen' => $this->elearningRepository->topVijfToetsenLaatsteJaar($product),
                'ga_laatste_maand' => array(
                    'jaar' => $this->dateTimeDispatcher->vierWekenGeleden()->format('Y'),
                    'maand' => $this->dateTimeDispatcher->vierWekenGeleden()->format('m'),
                    'dag' => $this->dateTimeDispatcher->vierWekenGeleden()->format('d'),
                    'aantal' => $this->googleRepository->getPageViews($product, $this->dateTimeDispatcher->vierWekenGeleden()->format('Y-m-d'), 'yesterday')),
                'kpi' => $this->kpi,
            ),
        );
        $this->view->render($response, 'dashboard.html.twig', $data);
    }

}
