<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 24-1-2017
 * Time: 12:02
 */

namespace App\Controllers\Nascholing;


use App\Models\Product;
use Slim\Http\Response;
use Slim\Views\Twig;

class UitgaveController{

    protected $view;
    protected $uitgaveRepository;

    public function __construct(Twig $view, UitgaveR $uitgaveRepository){
        $this->view = $view;
        $this->uitgaveRepository = $uitgaveRepository;
    }

    public function index(Response $response, $productCode){
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $uitgaves = Branding::find($product->swis_id)->uitgaves;

        $data = [
            'product' => $product,
            'uitgaves' => $uitgaves,
            'statistiek' => [
                'gemiddeld_unieke_gebruikers' => $this->uitgaveRepository->gemiddeldUniekeGebruikersByBrandingId($product->swis_id),
                'gemiddeld_aantal_pogingen' => 5,
                'statussen' => [
                    'uitgave_verwijderd' => 0,
                    'uitgave_bezig' => 0,
                    'uitgave_afgerond' => 0,
                    'uitgave_gefaald' => 0,
                ],
                'gemiddeld_percentage' => 67,
            ]
        ];

        $this->view->render($response, 'nascholing/uitgave/index.html.twig', $data);
    }
}