<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\Nascholing;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\ElearningRepository;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 */
class DefaultController extends Controller{

    protected $view;
    protected $elearningRepository;

    public function __construct(Twig $view, ElearningRepository $elearningRepository){
        $this->view = $view;
        $this->elearningRepository = $elearningRepository;
    }

    public function indexAction(Response $response, $productCode){
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $data = array(
            'product' => $product,
            'data' => array(
                'gemiddeld_verwerkt' => $this->elearningRepository->getGemiddeldVerwerkt($product),
                'gemiddeld_resultaat' => $this->elearningRepository->getGemiddeldeUitkomst($product),
                'artikelen' => $this->elearningRepository->alleToetsData($product)
            )
        );
        $this->view->render($response, 'nascholing/default.html.twig', $data);
    }
}
