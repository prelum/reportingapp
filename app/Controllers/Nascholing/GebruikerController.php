<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 24-1-2017
 * Time: 12:03
 */

namespace App\Controllers\Nascholing;


use Slim\Http\Response;

class GebruikerController{

    public function index(Response $response, $productCode){
        return 'Hier komt de pagina voor het gebruikersoverzicht van ' . $productCode;
    }
}