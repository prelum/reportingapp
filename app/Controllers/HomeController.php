<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Core\Controller;
use App\Repositories\SubscriptionRepository;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of HomeController
 *
 * @author thomas
 */
class HomeController extends Controller
{

    protected $subscriptionRepository;
    protected $view;

    public function __construct(Twig $view, SubscriptionRepository $subscriptionRepository)
    {
        $this->view = $view;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function indexAction(Request $request, Response $response)
    {
        $data = array(
            'data' => array(
                'prelum' => $this->subscriptionRepository->getOverzichtHuidigeAbonneeStand('Prelum uitgevers'),
                'accredidact' => $this->subscriptionRepository->getOverzichtHuidigeAbonneeStand('Accredidact'),
                'zwinq' => $this->subscriptionRepository->getOverzichtHuidigeAbonneeStand('Zwinq')
            )
        );
        $this->view->render($response, 'home.html.twig', $data);
    }

}
