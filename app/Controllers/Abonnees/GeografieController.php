<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\Abonnees;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\GeograficsRepository;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 */
class GeografieController extends Controller{

    protected $view;
    protected $geograficsRepository;

    public function __construct(Twig $view,  GeograficsRepository $geograficsRepository){
        $this->view = $view;
        $this->geograficsRepository = $geograficsRepository;
    }

    public function indexAction(Response $response, $productCode){
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $data = array(
            'product' => $product,
            'data' => array(
                'woonplaatsen' => $this->geograficsRepository->getWoonplaatsen($product),
                'provincies' => $this->geograficsRepository->getProvincies($product),
                'landen' => $this->geograficsRepository->getLanden($product)
            )
        );
        $this->view->render($response, 'geografie.html.twig', $data);
    }

}
