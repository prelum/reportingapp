<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\Abonnees;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\DemograficsRepository;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 */
class DemografieController extends Controller{

    protected $view;
    protected $demograficsRepository;

    public function __construct(Twig $view, DemograficsRepository $demograficsRepository){
        $this->view = $view;
        $this->demograficsRepository = $demograficsRepository;
    }

    public function indexAction(Response $response, $productCode){
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $data = array(
            'product' => $product,
            'data' => array(
                'beroepsverenigingen' => $this->demograficsRepository->getBeroepsverenigingen($product),
                'organisaties' => $this->demograficsRepository->getOrganisaties($product),
                'leeftijdsgroepen' => $this->demograficsRepository->getLeeftijdsgroepen($product)
            )
        );
        $this->view->render($response, 'demografie.html.twig', $data);
    }

}
