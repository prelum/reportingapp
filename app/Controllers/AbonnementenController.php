<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\SubscriptionRepository;
use Slim\Http\Response;
use Slim\Views\Twig;


/**
 * Description of DashboardController
 *
 * @author thomas
 */
class AbonnementenController extends Controller {

    protected $view;
    protected $subscriptionRepository;

    public function __construct(Twig $view,SubscriptionRepository $subscriptionRepository) {
        $this->view = $view;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function indexAction(Response $response, $productCode) {
        $product = Product::where(['datacode' => $productCode])->firstOrFail();

        list($verloop['start'],$verloop['dag']) = $this->subscriptionRepository->verloopActief($product, $product->getStartJaar(), new \DateTime());

        $startDate = new \DateTime();
        $startDate->setDate($verloop['start']['jaar'], $verloop['start']['maand'], $verloop['start']['dag']);

        $data = array(
            'product' => $product,
            'data' => array(
                'collectieven' => $this->subscriptionRepository->getCollectieven($product),
                'kortingsgroepen' => $this->subscriptionRepository->getKortingsgroepen($product),
                'verloop_laatste_maand' => array(
                    /* org: startdatum product opgehaald uit prelumwebsite
                     'jaar' => $product->getStartJaar()->format('Y'),
                    'maand' => $product->getStartJaar()->format('m'),
                    'dag' => $product->getStartJaar()->format('d'),
                     */
                    /* new: 2106-17 startdatum is datum 1ste reguliere abonnee */
                    'jaar' => $verloop['start']['jaar'],
                    'maand' => $verloop['start']['maand'],
                    'dag' => $verloop['start']['dag'],
                    'aantal' => $verloop['dag']),
                'inschrijvingen_laatste_maand' => array(
                    'jaar' => $verloop['start']['jaar'],
                    'maand' => $verloop['start']['maand'],
                    'dag' => $verloop['start']['dag'],
                    'aantal' => $this->subscriptionRepository->verloopStartActieveAbonnementen($product, $startDate, new \DateTime())),
                'uitschrijvingen_laatste_maand' => array(
                    'jaar' => $verloop['start']['jaar'],
                    'maand' => $verloop['start']['maand'],
                    'dag' => $verloop['start']['dag'],
                    'aantal' => $this->subscriptionRepository->verloopEindActieveAbonnementen($product, $startDate, new \DateTime())),
                'registraties_laatste_maand' => array(
                    'jaar' => $verloop['start']['jaar'],
                    'maand' => $verloop['start']['maand'],
                    'dag' => $verloop['start']['dag'],
                    'aantal' => $this->subscriptionRepository->verloopRegistraties($product, $startDate, new \DateTime())),
            )
        );

        $this->view->render($response, 'abonnementen.html.twig', $data);
    }

}
