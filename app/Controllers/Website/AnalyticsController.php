<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\Website;

use App\Core\Controller;
use App\Models\Product;
use App\Repositories\GoogleAnalyticsRepository;
use App\Services\DatetimeDispatcher;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Description of DashboardController
 *
 * @author thomas
 */
class AnalyticsController extends Controller {

    protected $view;
    protected $googleAnalyticsRepository;
    protected $dateTimeDispatcher;

    public function __construct(Twig $view, DatetimeDispatcher $datetimeDispatcher, GoogleAnalyticsRepository $googleAnalyticsRepository) {
        $this->view = $view;
        $this->dateTimeDispatcher = $datetimeDispatcher;
        $this->googleAnalyticsRepository = $googleAnalyticsRepository;
    }

    public function analyticsAction(Response $response, $productCode) {
        $product = Product::where(['datacode' => $productCode])->firstOrFail();
        $data = array(
            'product' => $product,
            'data' => array(
                'uniekebezoekers' => $this->googleAnalyticsRepository->getUniqueUsers($product, '28daysAgo', 'yesterday'),
                'gembezoekduur' => $this->googleAnalyticsRepository->getAverageSessionDuration($product, '28daysAgo', 'yesterday'),
                'bouncerate' => $this->googleAnalyticsRepository->getSingleVisitPercentage($product, '28daysAgo', 'yesterday'),
                'uniekegebruikers' => $this->googleAnalyticsRepository->getNewSessionsPercentage($product, '28daysAgo', 'yesterday'),
                'gembezoeken' => $this->googleAnalyticsRepository->getAverageSessionsPerUser($product, '28daysAgo', 'yesterday'),
                'bezoekenperpaginatitel' => $this->googleAnalyticsRepository->getUniquePageViews($product, '28daysAgo', 'yesterday'),
                'verwijzendeurls' => $this->googleAnalyticsRepository->getNavigationFrom($product, '28daysAgo', 'yesterday'),
                'populairezoekwoorden' => $this->googleAnalyticsRepository->getPopulaireZoekwoorden($product, '28daysAgo', 'yesterday'),
                'bezoekenperland' => $this->googleAnalyticsRepository->getCountries($product, '28daysAgo', 'yesterday'),
                'bezoekenperbrowser' => $this->googleAnalyticsRepository->getBrowsers($product, '28daysAgo', 'yesterday'),
                'webviews' => array(
                    'jaar' => $this->dateTimeDispatcher->vorigJaar()->format('Y'),
                    'maand' => $this->dateTimeDispatcher->vorigJaar()->format('m'),
                    'dag' => $this->dateTimeDispatcher->vorigJaar()->format('d'),
                    'aantal' => $this->googleAnalyticsRepository->getPageViews($product, $this->dateTimeDispatcher->vorigJaar()->format('Y-m-d'), 'yesterday')),
            )
        );
        $this->view->render($response, 'websiteanalytics.html.twig', $data);
    }
}
