<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 13:58
 */

namespace App\Services\Cleaner;

/**
 * Interface CleanerInterface
 * @package App\Services\Cleaner
 *
 * Interface voor cleaners
 */
interface CleanerInterface{
    /**
     * @param $data
     * @return mixed
     *
     * Functie om data te cleanen
     */
    public static function clean($data);
}