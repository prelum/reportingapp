<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30-1-2017
 * Time: 16:04
 */

namespace App\Services\Cleaner\Cleaners;


use App\Services\Cleaner\CleanerInterface;

class DateCleaner implements CleanerInterface{

    public static function clean($date){
        if($date === null){
            return null;
        }

        \DateTime::createFromFormat('Y-m-d', $date);
        $errors = \DateTime::getLastErrors();
        if(!empty($errors['warning_count'])){
            return null;
        }
        return $date;
    }

}