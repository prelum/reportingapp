<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 15:02
 */

namespace App\Services\Cleaner\Cleaners;


use App\Services\Cleaner\CleanerInterface;

class ConverteerGoedFoutCleaner implements CleanerInterface{

    public static function clean($data){
        if($data === null){
            return null;
        }

        if($data == 1){
            return true;
        }
        return false;
    }
}