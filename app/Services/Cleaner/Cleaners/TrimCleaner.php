<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 13:02
 */

namespace App\Services\Cleaner\Cleaners;


use App\Services\Cleaner\CleanerInterface;

class TrimCleaner implements CleanerInterface{

    public static function clean($string){
        if($string === null){
            return null;
        }
        return trim($string);
    }
}