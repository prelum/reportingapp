<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 10-2-2017
 * Time: 10:53
 */

namespace App\Services\Calculators;


use App\Models\Uitgave;
use App\Repositories\UitgaveResultatenRepository;

class UitgaveCalculator{

    protected $uitgaveRepository;

    public function __construct(UitgaveResultatenRepository $uitgaveRepository){
        $this->uitgaveRepository = $uitgaveRepository;
    }

    public function calculate(Uitgave $uitgave){
        $data = new \stdClass();
        $data->unieke_gebruikers = $this->uitgaveRepository->getUniekeAbonnementenAantalUitgave($uitgave);
        $data->gemiddelde_score = $this->uitgaveRepository->getGemiddeldeScoreAllePogingenUitgave($uitgave);
        $data->gemiddelde_score_laatste_poging = $this->uitgaveRepository->getGemiddeldeScoreLaatstePogingUitgave($uitgave);
        $data->gemiddelde_aantal_pogingen = $this->uitgaveRepository->getGemiddeldAantalPogingenUitgave($uitgave);
        $data->statussen_voortgang = $this->uitgaveRepository->getStatussenAllePogingenenUitgave($uitgave);
        $data->statussen_voortgang_laatste_poging = $this->uitgaveRepository->getStatussenLaatstePogingenUitgave($uitgave);
        return $data;
    }
}