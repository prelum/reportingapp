<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 11:23
 */

namespace App\Services\Calculators;


use App\Models\EindtoetsAntwoord;
use App\Repositories\EindtoetsAntwoordRepository;

class EindtoetsAntwoordCalculator{

    public $eindToetsAntwoordRepository;

    public function __construct(EindtoetsAntwoordRepository $eindtoetsAntwoordRepository){
        $this->eindToetsAntwoordRepository = $eindtoetsAntwoordRepository;
    }

    public function calculate(EindtoetsAntwoord $antwoord): \stdClass{
        $data['alle_pogingen'] = $this->eindToetsAntwoordRepository->getAantalAllePogingen($antwoord);
        $data['laatste_pogingen'] = $this->eindToetsAntwoordRepository->getAantalLaatstePoging($antwoord);

        return (object)$data;
    }

}