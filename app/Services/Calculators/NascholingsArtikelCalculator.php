<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 10-2-2017
 * Time: 10:53
 */

namespace App\Services\Calculators;


use App\Models\Nascholingsartikel;
use App\Repositories\NascholingsartikelResultatenRepository;

class NascholingsArtikelCalculator{

    private $resultatenRepository;

    public function __construct(NascholingsartikelResultatenRepository $resultatenRepository){
        $this->resultatenRepository = $resultatenRepository;
    }

    public function calculate(Nascholingsartikel $nascholingsartikel): \stdClass{
        $data = new \stdClass();
        $data->unieke_gebruikers = $this->resultatenRepository->getUniekeAbonnementenAantalNascholingsartikel($nascholingsartikel);
        $data->gemiddelde_score = $this->resultatenRepository->getGemiddeldeScoreLaatstePogingNascholingsartikel($nascholingsartikel);
        $data->gemiddelde_score_laatste_poging = $this->resultatenRepository->getGemiddeldeScoreAllePogingenNascholingsartikel($nascholingsartikel);
        $data->gemiddelde_aantal_pogingen = $this->resultatenRepository->getGemiddeldAantalPogingenNascholingsartikel($nascholingsartikel);
        $data->statussen_voortgang = $this->resultatenRepository->getStatussenAllePogingenenNascholingsartikel($nascholingsartikel);
        $data->statussen_voortgang_laatste_poging = $this->resultatenRepository->getStatussenLaatstePogingenNascholingsartikel($nascholingsartikel);
        return $data;
    }
}