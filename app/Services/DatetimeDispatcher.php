<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

/**
 * Description of DatetimeDispatcher
 *
 * @author thomas
 *
 *
 */
class DatetimeDispatcher {

    public function nu($asString = false) {
        $datum = new \DateTime();
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

    public function vorigJaar($asString = false) {
        $datum = new \DateTime();
        $interval = new \DateInterval('P1Y');
        $datum->sub($interval);
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

    public function beginDitJaar($asString = false) {
        $datum = new \DateTime();
        $jaar = $datum->format('Y');
        $datum->modify('first day of January' . $jaar);
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

    public function beginVolgendJaar($asString = false) {
        $datum = new \DateTime();
        $interval = new \DateInterval('P1Y');
        $datum->add($interval);
        $jaar = $datum->format('Y');
        $datum->modify('first day of January' . $jaar);
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

    public function beginVorigJaar($asString = false) {
        $datum = new \DateTime();
        $jaar = $datum->format('Y');
        $vorigjaar = $jaar - 1;
        $datum->modify('first day of January' . $vorigjaar);
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }
    
    public function zelfdeDagVorigJaar($asString = false) {
        $datum = new \DateTime();
        $datum->modify('-1 year');
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

    public function vierWekenGeleden($asString = false) {
        $datum = new \DateTime();
        $interval = new \DateInterval('P28D');
        $datum->sub($interval);
        if (!$asString) {
            return $datum;
        } else {
            return $datum->format('Y-m-d');
        }
    }

}
