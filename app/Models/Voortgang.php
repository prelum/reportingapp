<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 14:57
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Voortgang extends Model{
    protected $connection = 'reporting';
    protected $table = 'voortgangen';
    protected $fillable = [
        'abonnement_id',
        'uitgave_id',
        'nascholingsartikel_id',
        'voortgang_type',
        'status',
        'datum'
    ];

    public function nascholingsartikel(){
        return $this->belongsTo(Nascholingsartikel::class);
    }

    public function abonnement(){
        return $this->belongsTo(Abonnement::class);
    }

    public function uitgave(){
        return $this->belongsTo(Uitgave::class);
    }
}