<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:42
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EindtoetsAntwoord extends Model{

    protected $connection = 'reporting';
    protected $table = 'eindtoets_antwoorden';
    protected $fillable = [
        'id',
        'vraag_id',
        'antwoord',
        'goed_fout',
        'volgorde',
    ];

    public function eindtoetsAntwoord(){
        $this->belongsTo(EindtoetsAntwoord::class,'vraag_id', 'id');
    }
}