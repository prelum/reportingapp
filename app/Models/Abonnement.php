<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Subscription
 *
 * @author thomas
 */
class Abonnement extends Model{
    protected $connection = 'reporting';
    protected $table = 'abonnementen';

    protected $fillable = [
        'id',
        'mainUsrinfo_id',
        'datacode',
        'productCode',
        'userid',
        'inschrijving',
        'insertDate',
        'lastChangeDate',
        'status',
        'aAbosoort',
        'aKorting',
        'aColabo',
        'aBegindatum',
        'aEinddatum',
        'vAanhef',
        'vVoorletters',
        'vTussenvoegsels',
        'vNaam',
        'vAfdeling',
        'vOrganisatie',
        'vAdres',
        'vNummer',
        'vToevoeging',
        'vPostcodeNum',
        'vPostcodeChar',
        'vPostcodeBuitenland',
        'vWoonplaats',
        'vLand',
        'vGeboortedatum',
        'vEmail',
        'vBeroepsver'
    ];

    public function isRegistrationTypeSubscriptionType(){
        if($this->aAbosoort == $this->abonnementType){
            return true;
        }

        return false;
    }

    public function getFullName(){
        $nameArray = array_filter(array($this->vVoorletters, $this->vTussenvoegsels, $this->vNaam));
        return implode(' ', $nameArray);
    }

    public function voortgangen(){
        return $this->hasMany(Voortgang::class);
    }
}
