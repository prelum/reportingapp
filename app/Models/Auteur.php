<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:44
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Auteur extends Model{
    protected $connection = 'reporting';
    protected $table = 'auteurs';
    protected $fillable = [
        'id',
        'product_id',
        'branding_id',
        'auteur',
        'titel',
        'beschrijving'
    ];

    public function nascholingsartikelen(){
        return $this->belongsToMany(Nascholingsartikel::class, 'nascholingsartikels_auteurs');
    }

}