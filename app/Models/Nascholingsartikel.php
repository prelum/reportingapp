<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Nascholingsartikel extends Model{

    protected $connection = 'reporting';
    protected $table = 'nascholingsartikelen';
    protected $fillable = [
        'id',
        'product_id',
        'branding_id',
        'uitgave_id',
        'rubriek_id',
        'titel',
        'ondertitel',
        'samenvatting',
        'type',
        'vereiste_percentage',
        'aantal_pogingen',
        'publicatie_datum',
        'eind_datum',
        'ahead_of_publication_date',
        'online',
        'gaia_cursus_id',
        'gaia_module_id',
        'volgorde',
        'accreditatiepunt',
        'beoordeeld',
        'header_afbeelding'
    ];

    protected $casts = [
        'statussen_voortgang' => 'array',
        'statussen_voortgang_laatste_poging' => 'array',
    ];

    public function auteurs(){
        return $this->belongsToMany(Auteur::class, 'nascholingsartikels_auteurs')->withPivot('volgorde');
    }

    public function eindtoetsVragen(){
        return $this->hasMany(EindtoetsVraag::class);
    }

    public function eindtoetsAntwoorden(){
        return $this->hasManyThrough(EindtoetsAntwoord::class, EindtoetsVraag::class, 'nascholingsartikel_id', 'vraag_id');
    }

    public function uitgave(){
        return $this->belongsTo(Uitgave::class);
    }

    public function voortgangen(){
        return $this->hasMany(Voortgang::class);
    }
}