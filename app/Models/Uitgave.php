<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Uitgave extends Model{
    protected $connection = 'reporting';
    protected $table = 'uitgaves';
    protected $fillable = [
        'id',
        'product_id',
        'branding_id',
        'titel',
        'jaar',
        'jaargang',
        'vereiste_percentage',
        'aantal_pogingen',
        'publicatie_datum',
        'eind_datum',
        'accreditatiepunt',
        'unieke_gebruikers'
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function nascholingsartikelen(){
        return $this->hasMany(Nascholingsartikel::class);
    }

    public function voortgangen(){
        return $this->hasMany(Voortgang::class);
    }
}