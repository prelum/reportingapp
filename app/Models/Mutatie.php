<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30-1-2017
 * Time: 14:15
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Mutatie extends Model{

    protected $connection = 'reporting';
    protected $table = 'mutaties';

    protected $fillable = ['connection', 'table_name', 'table_record_id', 'table_action', 'checked'];


}