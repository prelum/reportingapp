<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:42
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EindtoetsVraag extends Model{
    protected $connection = 'reporting';
    protected $table = 'eindtoets_vragen';
    protected $fillable = [
        'id',
        'nascholingsartikel_id',
        'antwoord_template_id',
        'type_vraag',
        'vraag',
        'juiste_antwoord',
        'feedback',
        'feedback_verdieping_titel',
        'feedback_verdieping',
        'volgorde',
        'willekeurig_sorteren',
        'video',
        'video_id',
    ];

    public function nascholingsartikel(){
        return $this->belongsTo(Nascholingsartikel::class);
    }

    public function eindtoetsAntwoorden(){
        return $this->hasMany(EindtoetsAntwoord::class, 'vraag_id');
    }
}