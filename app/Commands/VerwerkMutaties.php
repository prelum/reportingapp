<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 11:21
 */

namespace App\Commands;


use App\Core\Dispatcher;
use App\Models\Mutatie;
use App\Repositories\ImportRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VerwerkMutaties extends Command{

    protected $dispatcher;

    protected $importRepository;

    protected $recalculate = [
        'nascholingsartikel_voortgang',
        'uitgave_voortgang'
    ];

    protected $recalculation_sets = [
        'uitgaves' => [],
        'nascholingsartikelen' => []
    ];

    public function __construct(Dispatcher $dispatcher, ImportRepository $importRepository){
        parent::__construct();
        $this->dispatcher = $dispatcher;
        $this->importRepository = $importRepository;
    }

    public function configure(){
        $this->setName('mutaties:update');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        foreach(Mutatie::where('checked', false)->cursor() as $mutatie){
            $event = "$mutatie->connection.$mutatie->table_name.$mutatie->table_action";
            $this->dispatcher->fire($event, [$mutatie]);
            $mutatie->checked = true;
            $mutatie->save();
            $this->checkVoorRecalculatie($mutatie);
        }
        $this->runRecalculationEvents();
    }

    protected function checkVoorRecalculatie(Mutatie $mutatie){
        if(!in_array($mutatie->table_name, $this->recalculate)){
            return;
        }

        if($mutatie->table_name == 'nascholingsartikel_voortgang'){
            $this->recalculateNascholingsartikelVoortgang($mutatie);
            return;
        }

        if($mutatie->table_name == 'uitgave_voortgang'){
            $this->recalculateUitgaveVoortgang($mutatie);
            return;
        }
    }

    protected function recalculateNascholingsartikelVoortgang(Mutatie $mutatie){
        $nascholingsartikelVoortgang = $this->importRepository->getNascholingsartikelVoortgang($mutatie->table_record_id);
        $nascholingsartikel = $this->importRepository->getNascholingsartikel($nascholingsartikelVoortgang['nascholingsartikel_id']);
        $uitgave = $this->importRepository->getUitgave($nascholingsartikel['uitgave_id']);

        if(empty($nascholingsartikelVoortgang)){
            return;
        }

        (in_array($nascholingsartikel['id'], $this->recalculation_sets['nascholingsartikelen']))
            ?: $this->recalculation_sets['nascholingsartikelen'][] = $nascholingsartikel['id'];
        (in_array($uitgave['id'], $this->recalculation_sets['uitgaves']))
            ?: $this->recalculation_sets['uitgaves'][] = $uitgave['id'];
    }

    protected function recalculateUitgaveVoortgang(Mutatie $mutatie){
        $uitgaveVoortgang = $this->importRepository->getUitgaveVoortgang($mutatie->table_record_id);
        if(empty($uitgaveVoortgang)){
            return;
        }

        (in_array($uitgaveVoortgang['uitgave_id'], $this->recalculation_sets['uitgaves']))
            ?: $this->recalculation_sets['uitgaves'][] = $uitgaveVoortgang['uitgave_id'];

        $nascholingsartikelen = $this->importRepository->getUitgaveNascholingsartikelen($uitgaveVoortgang['uitgave_id']);

        foreach($nascholingsartikelen as $nascholingsartikel){
            (in_array($nascholingsartikel['id'], $this->recalculation_sets['nascholingsartikelen']))
                ?: $this->recalculation_sets['nascholingsartikelen'][] = $nascholingsartikel['id'];
        }

    }

    protected function runRecalculationEvents(){
        foreach($this->recalculation_sets['nascholingsartikelen'] as $nascholingsartikel_id){
            $this->dispatcher->fire('recalculate.nascholingsartikel', [$nascholingsartikel_id]);
        }

        foreach($this->recalculation_sets['uitgaves'] as $uitgave_id){
            $this->dispatcher->fire('recalculate.uitgave', [$uitgave_id]);
        }
    }
}