<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 10:24
 */

namespace App\Commands\Analyseren;


use App\Models\Uitgave;
use App\Services\Calculators\UitgaveCalculator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AnalyseUitgaves extends Command{

    public $uitgaveCalculator;

    public function __construct(UitgaveCalculator $uitgaveCalculator){
        parent::__construct();
        $this->uitgaveCalculator = $uitgaveCalculator;
    }

    public function configure(){
        $this->setName('analyseren:uitgave')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Calculeer alles')
            ->addArgument('uitgave-id', InputArgument::REQUIRED, 'De uitgave-id om te hercalculeren');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        if($input->getOption('all')){
            Uitgave::chunk(10, function($uitgaves){
                $uitgaves->each(function(Uitgave $uitgave){
                    $this->single($uitgave);
                });
            });
            return;
        }

        if(null != $input->getArgument('uitgave-id')){
            $uitgave = Uitgave::find($input->getArgument('uitgave-id'));
            $this->single($uitgave);
            return;
        }

        throw new \Exception('Geen optie of argument gegeven');
    }

    protected function single(Uitgave $uitgave){
        $data = $this->uitgaveCalculator->calculate($uitgave);

        $uitgave->unieke_gebruikers = $data->unieke_gebruikers;
        $uitgave->gemiddelde_score = $data->gemiddelde_score;
        $uitgave->gemiddelde_score_laatste_poging = $data->gemiddelde_score_laatste_poging;
        $uitgave->gemiddelde_aantal_pogingen = $data->gemiddelde_aantal_pogingen;
        $uitgave->statussen_voortgang = $data->statussen_voortgang;
        $uitgave->statussen_voortgang_laatste_poging = $data->statussen_voortgang_laatste_poging;

        $uitgave->save();
    }
}