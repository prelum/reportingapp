<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 10:24
 */

namespace App\Commands\Analyseren;

use App\Models\Nascholingsartikel;
use App\Services\Calculators\EindtoetsAntwoordCalculator;
use App\Services\Calculators\NascholingsArtikelCalculator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AnalyseNascholingsartikel extends Command{

    public $nascholingsArtikelCalculator;
    public $eindtoetsAntwoordCalculator;

    public function __construct(NascholingsArtikelCalculator $nascholingsArtikelCalculator, EindtoetsAntwoordCalculator $eindtoetsAntwoordCalculator){
        parent::__construct();
        $this->nascholingsArtikelCalculator = $nascholingsArtikelCalculator;
        $this->eindtoetsAntwoordCalculator = $eindtoetsAntwoordCalculator;
    }

    public function configure(){
        $this->setName('analyseren:nascholingsartikel')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Calculeer alles')
            ->addArgument('nascholingsartikel-id', InputArgument::OPTIONAL, 'De nascholingsartikel-id om te hercalculeren');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        if($input->getOption('all')){
            Nascholingsartikel::chunk(10, function($nascholingsartikelen){
                $nascholingsartikelen->each(function(Nascholingsartikel $nascholingsartikel){
                    $this->single($nascholingsartikel);
                });
            });
            return;
        }

        if(null != $input->getArgument('nascholingsartikel-id')){
            $nascholingsartikel = Nascholingsartikel::find((int)$input->getArgument('nascholingsartikel-id'));
            $this->single($nascholingsartikel);
            return;
        }

        throw new \Exception('Geen optie of argument gegeven');
    }

    private function single(Nascholingsartikel $nascholingsartikel){
        foreach($nascholingsartikel->eindtoetsAntwoorden as $eindtoetsAntwoord){
            $resultaten = $this->eindtoetsAntwoordCalculator->calculate($eindtoetsAntwoord);
            $eindtoetsAntwoord->alle_pogingen = $resultaten->alle_pogingen;
            $eindtoetsAntwoord->laatste_pogingen = $resultaten->laatste_pogingen;
            $eindtoetsAntwoord->save();
        }
        $data = $this->nascholingsArtikelCalculator->calculate($nascholingsartikel);
        $nascholingsartikel->unieke_gebruikers = $data->unieke_gebruikers;
        $nascholingsartikel->gemiddelde_score = $data->gemiddelde_score;
        $nascholingsartikel->gemiddelde_score_laatste_poging = $data->gemiddelde_score_laatste_poging;
        $nascholingsartikel->gemiddelde_aantal_pogingen = $data->gemiddelde_aantal_pogingen;
        $nascholingsartikel->statussen_voortgang = $data->statussen_voortgang;
        $nascholingsartikel->statussen_voortgang_laatste_poging = $data->statussen_voortgang_laatste_poging;
        $nascholingsartikel->save();
    }
}