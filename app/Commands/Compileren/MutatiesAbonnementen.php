<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30-1-2017
 * Time: 13:57
 */

namespace App\Commands\Compileren;


use App\Models\Mutatie;
use App\Repositories\ImportRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MutatiesAbonnementen extends Command{
    protected $dataImportRepository;

    public function __construct(ImportRepository $dataImportRepository){
        parent::__construct();
        $this->dataImportRepository = $dataImportRepository;
    }

    public function configure(){
        $this->setName('compileren:mutaties-abonnementen')
            ->setDescription('Haalt de relevante abonnementsmutaties op voor verwerking')
            ->addOption('vanaf', null, InputOption::VALUE_REQUIRED, 'Vanaf waar de mutaties verwerkt moeten worden')
            ->addOption('tot', null, InputOption::VALUE_REQUIRED, 'Tot waar de mutaties verwerkt moeten worden');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $vanaf = new \DateTime($input->getOption('vanaf'));
        $tot = new \DateTime($input->getOption('tot'));
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($vanaf, $interval, $tot);

        foreach($period as $date){
            $this->verwerkDag($date);
        }


    }

    private function verwerkDag($date){
        $page = 0;
        while(count($this->dataImportRepository->getMainUserMutaties($date, $page)) > 0){
            $mainuser_mutaties = $this->dataImportRepository->getMainUserMutaties($date, $page);
            foreach($mainuser_mutaties as $mainuser){
                Mutatie::create([
                    'connection' => 'main',
                    'table_name' => 'mainUsrinfo',
                    'table_record_id' => $mainuser['id'],
                    'table_action' => 'composite',
                    'checked' => false
                ]);
            }
            $page++;
        }
    }

}