<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30-1-2017
 * Time: 13:57
 */

namespace App\Commands\Compileren;


use App\Models\Mutatie;
use App\Repositories\ImportRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MutatiesElearning extends Command{
    protected $dataImportRepository;

    public function __construct(ImportRepository $dataImportRepository){
        parent::__construct();
        $this->dataImportRepository = $dataImportRepository;
    }

    public function configure(){
        $this->setName('compileren:mutaties-elearning')
            ->setDescription('Haalt de relevante elearning mutaties op voor verwerking')
            ->addArgument('tables', InputArgument::IS_ARRAY, 'Verwerk alleen de aangegeven tabellen')
            ->addOption('vanaf', null, InputOption::VALUE_REQUIRED, 'Vanaf waar de mutaties verwerkt moeten worden')
            ->addOption('tot', null, InputOption::VALUE_REQUIRED, 'Tot waar de mutaties verwerkt moeten worden');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $tables = (array)$input->getArgument('tables');

        $vanaf = new \DateTime($input->getOption('vanaf'));
        $tot = new \DateTime($input->getOption('tot'));
        $interval = new \DateInterval('P1D');

        $period = new \DatePeriod($vanaf, $interval, $tot);

        foreach($period as $date){
            $this->verwerkDag($date, $tables);
        }
    }


    public function verwerkDag($date, $tables){
        $page = 0;
        while(count($this->dataImportRepository->getElearningMutaties($date, $tables, $page)) > 0){
            $elearning_mutaties = $this->dataImportRepository->getElearningMutaties($date, $tables, $page);
            foreach($elearning_mutaties as $mutatie){
                if($mutatie['table_record_id'] == null){
                    return;
                }
                Mutatie::create([
                    'connection' => 'elearning',
                    'table_name' => $mutatie['table_name'],
                    'table_record_id' => $mutatie['table_record_id'],
                    'table_action' => $mutatie['table_action'],
                    'checked' => false
                ]);
            }
            $page++;
        }
    }

}