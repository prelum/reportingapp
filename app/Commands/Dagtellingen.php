<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Commands;

use App\Models\Product;
use App\Repositories\DagtellingRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of DailyRegistrationsCountTask
 *
 * @author thomas
 */
class Dagtellingen extends Command{

    private $analysesRepository;

    public function __construct(DagtellingRepository $analysesRepository){
        parent::__construct();
        $this->analysesRepository = $analysesRepository;
    }

    public function configure(){
        $this->setName('analyseren:dagtellingen')
            ->addOption(
                'datum',
                null,
                InputOption::VALUE_OPTIONAL,
                'De startdatum waarover de telling gedaan wordt. Wanneer deze niet gegeven wordt neemt het systeem de begindatum per product.'
            )
            ->addOption(
                'producten',
                null,
                InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL,
                'Een array van producten waarover de telling gedaan dient te worden.'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $einde = new \DateTime('today');
        $interval = new \DateInterval('P1D');

        if(!empty($input->getOption('producten'))){
            $producten = Product::whereIn('datacode', $input->getOption('producten'))->get();
        }else{
            $producten = Product::all();
        }

        foreach($producten as $product){
            if(!empty($input->getOption('datum'))){
                $start = new \DateTime($input->getOption('datum'));
            }else{
                $start = new \DateTime($product->start_jaar);
            }
            $range = new \DatePeriod($start, $interval, $einde);
            foreach($range as $datum){
                $this->runCounts($datum, $product);
            }
        }
    }


    protected function runCounts(\DateTime $datum, Product $product){
        $this->analysesRepository->countRegistrations($datum, $product);
        $this->analysesRepository->countSubscriptionStarts($datum, $product);
        $this->analysesRepository->countActiveSubscriptions($datum, $product);
        $this->analysesRepository->countSubscriptionEnds($datum, $product);
    }

}
