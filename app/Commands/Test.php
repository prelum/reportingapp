<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 24-3-2017
 * Time: 11:41
 */

namespace App\Commands;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Test extends Command{
    public function configure(){
        $this->setName('test');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $date = new \DateTime();
        $string = 'Running test: ' . $date->format('Y-m-d H:i:s') . "\n";
        $output->write($string);
    }
}