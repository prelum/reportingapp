<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Database\Capsule\Manager;
use App\Models\Product;
use App\Core\Collection;

/**
 * Description of AbonnementenRepository
 *
 * @author thomas
 */
class SubscriptionRepository extends BaseRepository {

    public $reporting;

    public function __construct(Manager $manager)
    {
        $this->reporting = $manager->getConnection('reporting')->getPdo();
    }

    public function getStand(Product $product, \DateTime $datum) {
        $query = "SELECT COUNT(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum < :beginDatum
            AND (aEinddatum >= :eindDatum OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer'";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->bindValue(':beginDatum', $datum->format('Y-m-d'));
        $stmt->bindValue(':eindDatum', $datum->format('Y-m-d'));
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['aantal'];
    }

    public function getRegistrationsCount(Product $product, \DateTime $beginDatum, \DateTime $eindDatum) {
        $query = "SELECT COUNT(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND insertDate >= :beginDatum
            AND insertDate < :eindDatum
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer';";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':beginDatum' => $beginDatum->format('Y-m-d'), ':eindDatum' => $eindDatum->format('Y-m-d')));
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['aantal'];
    }

    public function getAboStarts(Product $product, \DateTime $beginDatum, \DateTime $eindDatum) {
        $query = "SELECT COUNT(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum >= :beginDatum
            AND aBegindatum < :eindDatum
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer';";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':beginDatum' => $beginDatum->format('Y-m-d'), ':eindDatum' => $eindDatum->format('Y-m-d')));
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['aantal'];
    }

    public function getAboEindes(Product $product, \DateTime $beginDatum, \DateTime $eindDatum) {
        $query = "SELECT COUNT(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND aEinddatum >= :beginDatum
            AND aEinddatum < :eindDatum
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer';";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':beginDatum' => $beginDatum->format('Y-m-d'), ':eindDatum' => $eindDatum->format('Y-m-d')));
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['aantal'];
    }

    public function getOverzichtHuidigeAbonneeStand($group) {
        $query = "
            SELECT producten.naam as naam, COUNT(*) as aantal
            FROM producten
            JOIN abonnementen ON abonnementen.datacode = producten.datacode
            WHERE aKorting NOT LIKE  '%100%'
            AND supergroep = :group
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer'
            AND aBegindatum <=NOW()
            AND (aEinddatum > NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            GROUP BY abonnementen.datacode;
            ";
        $connection = $this->reporting;
        $stmt = $connection->prepare($query);
        $stmt->execute(array(':group' => $group));
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data['naam'][] = $row['naam'];
            $data['aantal'][] = (int) $row['aantal'];
        }
        return $data;
    }

    public function getCollectieven(Product $product) {
        $query = "
            SELECT aColabo, count(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum >= NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND (aColabo <> '' OR aColabo IS NOT NULL)
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer'
            AND aKorting NOT LIKE '100%'
            AND aColabo <> ''
            GROUP BY aColabo;
           ";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('titel' => $row['aColabo'], 'aantal' => $row['aantal']);
        }
        return $data;
    }

    public function getKortingsgroepen(Product $product) {
        $query = "
            SELECT aKorting, count(*) as aantal
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum >= NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND status = 'actief'
            AND aAbosoort <> 'proefnummer'
            AND aKorting NOT LIKE '100%'
            AND aColabo = ''
            GROUP BY aKorting;
           ";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('titel' => $row['aKorting'], 'aantal' => $row['aantal']);
        }

        return $data;
    }

    public function getSubscriptions(Product $product, $filterQuery = '') {
        $query = " 
            SELECT *
            FROM abonnementen
            LEFT JOIN inschrijvingen ON inschrijvingen.inschrijvingID = abonnementen.inschrijving AND abonnementen.productCode = inschrijvingen.productCode
            WHERE abonnementen.datacode = :datacode
            $filterQuery;";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'App\Models\Subscription');
        $subscriptions = new Collection();
        while ($row = $stmt->fetch()) {
            $subscriptions->set($row);
        }
        return $subscriptions;
    }
    
    public function verloopActief(Product $product, \DateTime $begindatum, \DateTime $einddatum) {
        $query = "
            SELECT YEAR(datum) as 'jaar', MONTH(datum) as 'maand', DAY(datum) as 'dag', SUM(aantal) as aantal
            FROM data_abonnementen_actief
            WHERE datacode = :datacode
            AND datum >= :begindatum
            AND datum < :einddatum
            AND aKorting NOT LIKE '%100%'
            AND aAbosoort <> 'proefnummer'
            GROUP BY datum
            ORDER BY datum;";

        $stmt = $this->reporting->prepare($query);

        $stmt->execute(array(':datacode' => $product->datacode, ':begindatum' => $begindatum->format('Y-m-d'), ':einddatum' => $einddatum->format('Y-m-d')));
        $data = array();
        /* eerste record bevat startdatum, eerste reguliere abonnee van product*/
        $startDate = $stmt->fetch(\PDO::FETCH_ASSOC);
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = (int) $row['aantal'];
        }
        return array($startDate,$data);
    }
    public function verloopActiefStart(Product $product, \DateTime $begindatum, \DateTime $einddatum) {
        $query = "
            SELECT YEAR(datum) as 'jaar', MONTH(datum) as 'maand', DAY(datum) as 'dag', SUM(aantal) as aantal
            FROM data_abonnementen_actief
            WHERE datacode = :datacode
            AND datum >= :begindatum
            AND datum < :einddatum
            AND aKorting NOT LIKE '%100%'
            AND aAbosoort <> 'proefnummer'
            GROUP BY datum
            ORDER BY datum;";

        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':begindatum' => $begindatum->format('Y-m-d'), ':einddatum' => $einddatum->format('Y-m-d')));
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }
    public function verloopStartActieveAbonnementen(Product $product, \DateTime $begindatum, \DateTime $einddatum) {
        $query = "
            SELECT YEAR(calendar_table.dt) as jaar, MONTH(calendar_table.dt) as maand ,DAY(calendar_table.dt) as dag, IFNULL(inschrijvingen.aantal, 0) as aantal
            FROM calendar_table
            LEFT JOIN (
                    SELECT datum, SUM(aantal) as aantal
                    FROM data_abonnementen_starts
                    WHERE datacode = :datacode
                    AND aKorting NOT LIKE '%100%'
                    AND aAbosoort <> 'proefnummer'
                    GROUP BY datum
                    ) as inschrijvingen ON calendar_table.dt = inschrijvingen.datum
            WHERE calendar_table.dt >= :begindatum
            AND calendar_table.dt < :einddatum
            ORDER BY calendar_table.dt;";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':begindatum' => $begindatum->format('Y-m-d'), ':einddatum' => $einddatum->format('Y-m-d')));
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = (int) $row['aantal'];
        }
        return $data;
    }

    public function verloopRegistraties(Product $product, \DateTime $begindatum, \DateTime $einddatum) {
        $query = "
            SELECT YEAR(calendar_table.dt) as jaar, MONTH(calendar_table.dt) as maand ,DAY(calendar_table.dt) as dag, IFNULL(inschrijvingen.aantal, 0) as aantal
            FROM calendar_table
            LEFT JOIN (
                    SELECT datum, SUM(aantal) as aantal
                    FROM data_abonnementen_registraties
                    WHERE datacode = :datacode
                    AND aKorting NOT LIKE '%100%'
                    AND aAbosoort <> 'proefnummer'
                    GROUP BY datum
                    ) as inschrijvingen ON calendar_table.dt = inschrijvingen.datum
            WHERE calendar_table.dt >= :begindatum
            AND calendar_table.dt < :einddatum
            ORDER BY calendar_table.dt;";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':begindatum' => $begindatum->format('Y-m-d'), ':einddatum' => $einddatum->format('Y-m-d')));
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = (int) $row['aantal'];
        }
        return $data;
    }

    public function verloopEindActieveAbonnementen(Product $product, \DateTime $begindatum, \DateTime $einddatum) {
        $query = "
            SELECT YEAR(calendar_table.dt) as jaar, MONTH(calendar_table.dt) as maand ,DAY(calendar_table.dt) as dag, IFNULL(uitschrijvingen.aantal, 0) as aantal
            FROM calendar_table
            LEFT JOIN (
                    SELECT datum, SUM(aantal) as aantal
                    FROM data_abonnementen_eindes
                    WHERE datacode = :datacode
                    AND aKorting NOT LIKE '%100%'
                    AND aAbosoort <> 'proefnummer'
                    GROUP BY datum
                    ) as uitschrijvingen ON calendar_table.dt = uitschrijvingen.datum
            WHERE calendar_table.dt >= :begindatum
            AND calendar_table.dt < :einddatum
            ORDER BY calendar_table.dt
            ;";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute(array(':datacode' => $product->datacode, ':begindatum' => $begindatum->format('Y-m-d'), ':einddatum' => $einddatum->format('Y-m-d')));
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = (int) $row['aantal'];
        }
        return $data;
    }
}
