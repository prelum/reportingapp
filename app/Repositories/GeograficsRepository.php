<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Database\Capsule\Manager;
use App\Models\Product;

/**
 * Description of AbonnementenRepository
 *
 * @author thomas
 */
class GeograficsRepository extends BaseRepository
{

    private $reporting;

    public function __construct(Manager $manager)
    {
        $this->reporting = $manager->getConnection('reporting')->getPdo();
    }

    public function getWoonplaatsen(Product $product) {
        $query = "SELECT vWoonplaats, COUNT(*) as aantal 
            FROM abonnementen 
            WHERE datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum >= NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            AND vLand = 'NEDERLAND'
            AND status = 'actief'
            GROUP BY vWoonplaats;";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('name' => ucwords(strtolower($row['vWoonplaats'])), 'value' => $row['aantal']);
        }
        return $data;
    }

    public function getProvincies(Product $product) {
        $provincies = $this->getPostcodeNumToProvince();
        $query = "SELECT vPostcodeNum, COUNT(*) as aantal 
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum >= NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            AND vLand = 'NEDERLAND'
            AND status = 'actief'
            GROUP BY vPostcodeNum;";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if(!array_key_exists($row['vPostcodeNum'], $provincies)){
                continue;
            }
            $provincie = $provincies[$row['vPostcodeNum']];
            $aantal = $row['aantal'];
            if (!array_key_exists($provincie, $data)) {
                $data[$provincie] = $aantal;
            } else {
                $data[$provincie] = $data[$provincie] + $aantal;
            }
        }
        $hc_keys = $this->getHCKeysProvince();
        foreach ($data as $prov => $aantal) {
            $dataset[] = array('hc-key' => $hc_keys[$prov], 'value' => $aantal);
        }
        return $dataset;
    }

    public function getPostcodeNumToProvince() {
        $query = "SELECT province, pnum FROM referentie_postcodes GROUP BY pnum";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $result[$row['pnum']] = $row['province'];
        }
        return $result;
    }

    public function getHCKeysProvince() {
        $query = "SELECT province, province_code FROM referentie_postcodes GROUP BY province_code";
        $stmt = $this->reporting->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $result[$row['province']] = "nl-" . strtolower($row['province_code']);
        }
        return $result;
    }

    public function getLanden(Product $product) {
        $query = "SELECT vLand, COUNT(*) as aantal 
            FROM abonnementen
            WHERE datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum >= NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            GROUP BY vLand";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('titel' => ucwords(strtolower($row['vLand'])), 'aantal' => $row['aantal']);
        }
        return $data;
    }

}
