<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Database\Capsule\Manager;
use App\Models\Product;

/**
 * Description of ToetsenRepository
 *
 * @author thomas
 */
class ElearningRepository extends BaseRepository
{

    private $main;

    public function __construct(Manager $manager)
    {
        $this->main = $manager->getConnection('main')->getPdo();
    }

    public function topVijfToetsenLaatsteJaar(Product $product) {
        $uitgave = '';
        $qpgroep = '';
        if ($product->productCode == 'ACC') {
            $uitgave = " AND elearningToetsing.uitgave = '" . $product->uitgave . "' ";
        }
        if ($product->productCode == 'QP') {

            $qpgroep = " AND mainSendinfo.vBeroepsver = '" . $product->vBeroepsver . "' ";
        }
        $query = "SELECT elearningToetsing.titel, count(elearningKennisfin.userid) as aantal, ROUND(AVG(elearningKennisfin.percentage),1) as gemiddelde
            FROM elearningToetsing
            JOIN elearningKennisfin ON elearningKennisfin.nascholingsid = elearningToetsing.nascholingsid
            JOIN mainUsrinfo ON elearningKennisfin.userid = mainUsrinfo.userid
            JOIN mainSendinfo ON mainUsrinfo.sendref = mainSendinfo.sendref
            WHERE elearningToetsing.productCode = :productCodeToetsing
            AND elearningKennisfin.productCode = :productCodeKennisfin
            $uitgave
            $qpgroep
            AND DATE(STR_TO_DATE(elearningKennisfin.datum, '%Y-%m-%d %H:%i:%s')) > (DATE(NOW()) - INTERVAL 1 YEAR)
            AND (elearningToetsing.PeCourseId <> '' AND elearningToetsing.PeCourseId <> '0' AND elearningToetsing.PeCourseId <> '1' AND elearningToetsing.PeCourseId <> '0000' AND elearningToetsing.PeCourseId <> '00000' AND elearningToetsing.PeCourseId <> '000000' AND elearningToetsing.PeCourseId <> '0000000' AND elearningToetsing.PeCourseId <> '0000000000')
            GROUP BY elearningToetsing.titel
            ORDER BY aantal DESC
            LIMIT 5;";
        $stmt = $this->main->prepare($query);
        $stmt->execute(['productCodeToetsing' => $product->productCode, 'productCodeKennisfin' => $product->productCode]);
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    public function alleToetsData(Product $product) {
        $uitgave = '';
        $qpgroep = '';
        if ($product->uitgave != '') {
            $uitgave = " AND elearningToetsing.uitgave = '" . $product->uitgave . "' ";
        }
        if ($product->productCode == 'QP') {
            $qpgroep = " AND mainSendinfo.vBeroepsver = '" . $product->vBeroepsver . "' ";
        }
        $query = "
            SELECT elearningToetsing.nascholingsid, elearningToetsing.titel, elearningToetsing.uitgave, COUNT(elearningKennisfin.userid) as aantal, ROUND(AVG(elearningKennisfin.percentage),1) as gemiddelde, startdatum, einddatum, elearningToetsing.PeCourseId, elearningToetsing.PeModuleId
            FROM elearningToetsing
            JOIN elearningKennisfin ON elearningKennisfin.nascholingsid = elearningToetsing.nascholingsid
            JOIN mainUsrinfo ON elearningKennisfin.userid = mainUsrinfo.userid
            JOIN mainSendinfo ON mainUsrinfo.sendref = mainSendinfo.sendref
            WHERE elearningToetsing.productCode = :productCode
            AND (elearningToetsing.PeCourseId <> '' AND elearningToetsing.PeCourseId <> '0' AND elearningToetsing.PeCourseId <> '1' AND elearningToetsing.PeCourseId <> '0000' AND elearningToetsing.PeCourseId <> '00000' AND elearningToetsing.PeCourseId <> '000000' AND elearningToetsing.PeCourseId <> '0000000' AND elearningToetsing.PeCourseId <> '0000000000')
            $uitgave
            $qpgroep
            GROUP BY elearningToetsing.nascholingsid
            ";
        $stmt = $this->main->prepare($query);
        $stmt->bindValue(':productCode', $product->productCode);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getGemiddeldVerwerkt($product) {
        $uitgave = '';
        $qpgroep = '';
        if ($product->uitgave != '') {
            $uitgave = " AND elearningToetsing.uitgave = '" . $product->uitgave . "' ";
        }
        if ($product->productCode == 'QP') {
            $qpgroep = " AND mainSendinfo.vBeroepsver = '" . $product->vBeroepsver . "' ";
        }
        $query = "
            SELECT COUNT(elearningKennisfin.userid) as aantal
            FROM elearningToetsing
            JOIN elearningKennisfin ON elearningKennisfin.nascholingsid = elearningToetsing.nascholingsid
            JOIN mainUsrinfo ON elearningKennisfin.userid = mainUsrinfo.userid
            JOIN mainSendinfo ON mainUsrinfo.sendref = mainSendinfo.sendref
            WHERE elearningToetsing.productCode = :productCode
            $uitgave
            $qpgroep
            GROUP BY elearningToetsing.nascholingsid;
            ";
        $stmt = $this->main->prepare($query);
        $stmt->bindValue(':productCode', $product->productCode);
        $stmt->execute();
        $aantal = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $aantal[] = $row['aantal'];
        }
        return round(array_sum($aantal) / count($aantal));
    }

    public function getGemiddeldeUitkomst($product) {
        $uitgave = '';
        $qpgroep = '';
        if ($product->uitgave != '') {
            $uitgave = " AND elearningToetsing.uitgave = '" . $product->uitgave . "' ";
        }
        if ($product->productCode == 'QP') {
            $qpgroep = " AND mainSendinfo.vBeroepsver = '" . $product->vBeroepsver . "' ";
        }
        $query = "
            SELECT ROUND(AVG(elearningKennisfin.percentage),1) as gemiddelde
            FROM elearningToetsing
            JOIN elearningKennisfin ON elearningKennisfin.nascholingsid = elearningToetsing.nascholingsid
            JOIN mainUsrinfo ON elearningKennisfin.userid = mainUsrinfo.userid
            JOIN mainSendinfo ON mainUsrinfo.sendref = mainSendinfo.sendref
            WHERE elearningToetsing.productCode = :productCode
            $uitgave
            $qpgroep;
            ";
        $stmt = $this->main->prepare($query);
        $stmt->bindValue(':productCode', $product->productCode);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row['gemiddelde'];
    }

}
