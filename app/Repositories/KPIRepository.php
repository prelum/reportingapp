<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Models\Product;
use App\Services\DatetimeDispatcher;

/**
 * Description of KPIgenerator
 *
 * @author thomas
 */
class KPIRepository {

    protected $product;
    protected $dateTimeDispatcher;
    protected $subscriptionRepository;
    protected $huidigestand;
    protected $standstartditjaar;
    protected $standstartvorigjaar;
    protected $prognoseeindstand;
    protected $registratiestotvandaag;
    protected $registratiesvorigeperiode;
    protected $aanmeldingentotvandaag;
    protected $aanmeldingentoteindjaar;
    protected $opzeggingentotvandaag;
    protected $opzeggingentoteindjaar;
    protected $groeivorigjaar;
    protected $groeiditjaar;

    public function __construct(DatetimeDispatcher $datetimeDispatcher, SubscriptionRepository $subscriptionRepository) {
        $this->dateTimeDispatcher = $datetimeDispatcher;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function setProduct(Product $product) {
        $this->huidigestand = $this->subscriptionRepository->getStand($product, $this->dateTimeDispatcher->nu());
        $this->standstartditjaar = $this->subscriptionRepository->getStand($product, $this->dateTimeDispatcher->beginDitJaar());
        $this->standstartvorigjaar = $this->subscriptionRepository->getStand($product, $this->dateTimeDispatcher->beginVorigJaar());
        $this->prognoseeindstand = $this->subscriptionRepository->getStand($product, $this->dateTimeDispatcher->beginVolgendJaar());
        $this->registratiestotvandaag = $this->subscriptionRepository->getRegistrationsCount($product, $this->dateTimeDispatcher->beginDitJaar(), $this->dateTimeDispatcher->nu());
        $this->registratiesvorigeperiode = $this->subscriptionRepository->getRegistrationsCount($product, $this->dateTimeDispatcher->beginVorigJaar(), $this->dateTimeDispatcher->zelfdeDagVorigJaar());
        $this->aanmeldingentotvandaag = $this->subscriptionRepository->getAboStarts($product, $this->dateTimeDispatcher->beginDitJaar(), $this->dateTimeDispatcher->nu());
        $this->aanmeldingentoteindjaar = $this->subscriptionRepository->getAboStarts($product, $this->dateTimeDispatcher->nu(), $this->dateTimeDispatcher->beginVolgendJaar());
        $this->opzeggingentotvandaag = $this->subscriptionRepository->getAboEindes($product, $this->dateTimeDispatcher->beginDitJaar(), $this->dateTimeDispatcher->nu());
        $this->opzeggingentoteindjaar = $this->subscriptionRepository->getAboEindes($product, $this->dateTimeDispatcher->nu(), $this->dateTimeDispatcher->beginVolgendJaar());
        if ($this->standstartvorigjaar == 0) {
            $this->groeivorigjaar = 100;
        } else {
            $this->groeivorigjaar = round(($this->standstartditjaar - $this->standstartvorigjaar) / $this->standstartvorigjaar * 100, 1);
        }
        if ($this->standstartditjaar == 0) {
            $this->groeiditjaar = 100;
        } else {
            $this->groeiditjaar = round(( $this->prognoseeindstand - $this->standstartditjaar) / $this->standstartditjaar * 100, 1);
        }
    }

    function getHuidigestand() {
        return $this->huidigestand;
    }

    function getStandstartditjaar() {
        return $this->standstartditjaar;
    }

    function getStandstartvorigjaar() {
        return $this->standstartvorigjaar;
    }

    function getPrognoseeindstand() {
        return $this->prognoseeindstand;
    }

    function getRegistratiestotvandaag() {
        return $this->registratiestotvandaag;
    }

    function getRegistratiesPeriodeVorigJaar() {
        return $this->registratiesvorigeperiode;
    }

    function verschilRegistraties(){
        if(($this->registratiestotvandaag - $this->registratiesvorigeperiode) == 0 || $this->registratiestotvandaag == 0){
            return 0;
        }
        return ($this->registratiestotvandaag - $this->registratiesvorigeperiode) / $this->registratiestotvandaag * 100;
    }

    function getActivatiestotvandaag() {
        return $this->aanmeldingentotvandaag;
    }

    function getActivatiestoteindjaar() {
        return $this->aanmeldingentoteindjaar;
    }

    function getOpzeggingentotvandaag() {
        return $this->opzeggingentotvandaag;
    }

    function getOpzeggingentoteindjaar() {
        return $this->opzeggingentoteindjaar;
    }

    function getGroeivorigjaar() {
        return $this->groeivorigjaar;
    }

    function getGroeiditjaar() {
        return $this->groeiditjaar;
    }

}
