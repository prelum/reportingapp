<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Database\Capsule\Manager;
use App\Models\Product;

/**
 * Description of DemografieRepository
 *
 * @author thomas
 */
class DemograficsRepository extends BaseRepository
{

    private $reporting;

    public function __construct(Manager $manager)
    {
        $this->reporting = $manager->getConnection('reporting')->getPdo();
    }

    public function getBeroepsverenigingen(Product $product) {
        $query = "
            SELECT vBeroepsver, count(*) as aantal
            FROM abonnementen
            WHERE (vBeroepsver <> '' AND vBeroepsver <> '0')
            AND datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum > NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            GROUP BY vBeroepsver
            HAVING aantal > 1;
           ";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('titel' => $row['vBeroepsver'], 'aantal' => $row['aantal']);
        }
        return $data;
    }

    public function getLeeftijdsgroepen(Product $product) {
        $query = "
            SELECT CASE
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) <= 20 THEN '<21'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=21 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=25 THEN '21-25'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=26 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=30 THEN '26-30'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=31 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=35 THEN '31-35'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=36 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=40 THEN '36-40'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=41 AND YEAR(NOW()) - YEAR(vGeboortedatum) <= 45 THEN '41-45'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=46 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=50 THEN '46-50'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=51 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=55 THEN '51-55'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=56 AND YEAR(NOW()) - YEAR(vGeboortedatum) <=60 THEN '56-60'
                WHEN YEAR(NOW()) - YEAR(vGeboortedatum) >=61 THEN '>61'
            END AS leeftijdsgroep,
            SUM(CASE WHEN vAanhef = 'Dhr.' THEN -1 ELSE 0 END) AS 'Man',
            SUM(CASE WHEN vAanhef = 'Mevr.' THEN 1 ELSE 0 END) AS 'Vrouw'
            FROM abonnementen
            WHERE datacode = :datacode
            AND (vGeboortedatum IS NOT NULL AND vGeboortedatum <> '0000-00-00')
            AND aBegindatum < NOW()
            AND (aEinddatum > NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND status = 'actief'
            GROUP BY leeftijdsgroep
            HAVING leeftijdsgroep IS NOT NULL
            ORDER BY vGeboortedatum DESC;
            ";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();

        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data['leeftijdsgroep'][] = $row['leeftijdsgroep'];
            $data['man'][] = (int) $row['Man'];
            $data['vrouw'][] = (int) $row['Vrouw'];
        }
        return $data;
    }

    public function getOrganisaties(Product $product) {
        $query = "
            SELECT vAfdeling, vOrganisatie, count(*) as aantal
            FROM abonnementen
            WHERE vOrganisatie <> ''
            AND datacode = :datacode
            AND aBegindatum < NOW()
            AND (aEinddatum > NOW() OR aEinddatum IS NULL OR aEinddatum = '0000-00-00')
            AND aKorting NOT LIKE '%100%'
            AND status = 'actief'
            GROUP BY vOrganisatie
            HAVING aantal > 1
            ORDER BY aantal DESC;
           ";
        $stmt = $this->reporting->prepare($query);
        $stmt->bindValue(':datacode', $product->datacode);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = array('titel' => $row['vOrganisatie'], 'afdeling' => $row['vAfdeling'], 'aantal' => $row['aantal']);
        }

        return $data;
    }

}
