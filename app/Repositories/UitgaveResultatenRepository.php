<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 12:16
 */

namespace App\Repositories;

use App\Models\Nascholingsartikel;
use App\Models\Uitgave;
use Illuminate\Database\Capsule\Manager;

class UitgaveResultatenRepository extends BaseRepository{
    public function __construct(Manager $manager){
        $this->reporting = $manager->getConnection('elearning')->getPdo();
    }

    public function getGemiddeldeScoreLaatstePogingUitgave(Uitgave $uitgave){
        $nascholingsartikelen = $uitgave->nascholingsartikelen->filter(function(Nascholingsartikel $nascholingsartikel){
            return !empty($nascholingsartikel->gemiddelde_score_laatste_poging);
        });

        $gemiddeldeScores = $nascholingsartikelen->pluck('gemiddelde_score_laatste_poging');

        return sum($gemiddeldeScores) / count($gemiddeldeScores);
    }

    public function getGemiddeldeScoreAllePogingenUitgave(Uitgave $uitgave){
        $nascholingsartikelen = $uitgave->nascholingsartikelen->filter(function(Nascholingsartikel $nascholingsartikel){
            return !empty($nascholingsartikel->gemiddelde_score);
        });

        $gemiddeldeScores = $nascholingsartikelen->pluck('gemiddelde_score');

        return sum($gemiddeldeScores) / count($gemiddeldeScores);
    }

    public function getGemiddeldAantalPogingenUitgave(Uitgave $uitgave){
        $nascholingsartikelen = $uitgave->nascholingsartikelen->filter(function(Nascholingsartikel $nascholingsartikel){
            return !empty($nascholingsartikel->gemiddelde_aantal_pogingen);
        });

        $gemiddeldAantalPogingen = $nascholingsartikelen->pluck('gemiddelde_aantal_pogingen');

        return sum($gemiddeldAantalPogingen) / count($gemiddeldAantalPogingen);
    }

    public function getUniekeAbonnementenAantalUitgave(Uitgave $uitgave){
        $abonnementen = $uitgave->voortgangen->filter(function(Voortgang $voortgang){
            return !empty($voortgang->abonnement);
        });
        return $abonnementen->count();
    }

    public function getStatussenAllePogingenenUitgave(Uitgave $uitgave){
        $nascholingsartikelen = $uitgave->nascholingsartikelen->filter(function(Nascholingsartikel $nascholingsartikel){
            return !empty($nascholingsartikel->statussen_voortgang);
        });

        $statussen = $nascholingsartikelen->pluck('statussen_voortgang');
        return keyedArraySumAndAverage($statussen);

    }

    public function getStatussenLaatstePogingenUitgave(Uitgave $uitgave){
        $nascholingsartikelen = $uitgave->nascholingsartikelen->filter(function(Nascholingsartikel $nascholingsartikel){
            return !empty($nascholingsartikel->statussen_voortgang_laatste_poging);
        });

        $statussen = $nascholingsartikelen->pluck('statussen_voortgang_laatste_poging');
        return keyedArraySumAndAverage($statussen);
    }
}