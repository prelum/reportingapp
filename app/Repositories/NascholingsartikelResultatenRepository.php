<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 27-2-2017
 * Time: 13:47
 */

namespace App\Repositories;


use App\Models\Nascholingsartikel;
use App\Models\Voortgang;
use Illuminate\Database\Capsule\Manager;

class NascholingsartikelResultatenRepository extends BaseRepository{
    public function __construct(Manager $manager){
        $this->reporting = $manager->getConnection('elearning')->getPdo();
    }

    public function getGemiddeldeScoreLaatstePogingNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $sql = "
            SELECT AVG(gemiddelde) / :aantal_vragen as gemiddelde
            FROM (
                SELECT front_gebruiker_id, SUM(gemiddeldes.vraag_correct) as gemiddelde FROM
                   (
                      SELECT eindtoets_gebruiker_vraag_antwoord.* 
                      FROM eindtoets_gebruiker_vraag_antwoord
                      JOIN (
                           SELECT nascholingsartikel_id, front_gebruiker_id, MAX(poging) as poging
                           FROM eindtoets_gebruiker_vraag_antwoord
                           WHERE eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = :nascholingsartikel_id
                           GROUP BY front_gebruiker_id
                      ) as maximum ON eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id = maximum.front_gebruiker_id AND eindtoets_gebruiker_vraag_antwoord.poging = maximum.poging AND eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = maximum.nascholingsartikel_id
                      WHERE status_voortgang <> 'eindtoets_bezig'
                      GROUP BY eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id, eindtoets_gebruiker_vraag_antwoord.eindtoets_vraag_id
                   ) as gemiddeldes
                GROUP BY front_gebruiker_id
            ) as totaal_gemiddelde
        ";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['nascholingsartikel_id' => $nascholingsartikel->id, 'aantal_vragen' => $nascholingsartikel->eindtoetsVragen->count()]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['gemiddelde'];
    }

    public function getGemiddeldeScoreAllePogingenNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $sql = "
            SELECT AVG(gemiddelde) / :aantal_vragen as gemiddelde
            FROM (
                SELECT front_gebruiker_id, SUM(gemiddeldes.vraag_correct) as gemiddelde FROM
                   (
                      SELECT eindtoets_gebruiker_vraag_antwoord.* 
                      FROM eindtoets_gebruiker_vraag_antwoord
                      WHERE status_voortgang <> 'eindtoets_bezig'
                      AND eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = :nascholingsartikel_id
                      GROUP BY eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id, eindtoets_gebruiker_vraag_antwoord.eindtoets_vraag_id
                   ) as gemiddeldes
                GROUP BY front_gebruiker_id
            ) as totaal_gemiddelde
        ";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['nascholingsartikel_id' => $nascholingsartikel->id, 'aantal_vragen' => $nascholingsartikel->eindtoetsVragen->count()]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['gemiddelde'];
    }

    public function getGemiddeldAantalPogingenNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $sql = "
            SELECT AVG(max_poging) as gemiddelde                                 
                FROM (                                                               
                SELECT MAX(poging) as max_poging                                     
                FROM eindtoets_gebruiker_vraag_antwoord                              
                WHERE eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = :nascholingsartikel_id
                GROUP BY front_gebruiker_id
            ) as max_pogingen
        ";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['nascholingsartikel_id' => $nascholingsartikel->id]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['gemiddelde'];
    }

    public function getUniekeAbonnementenAantalNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $abonnementen = $nascholingsartikel->voortgangen->filter(function(Voortgang $voortgang){
            return !empty($voortgang->abonnement);
        });
        return $abonnementen->count();
    }

    public function getStatussenAllePogingenenNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $sql = "
            SELECT voortgangen.status_voortgang, COUNT(*) as aantal
            FROM (
                SELECT eindtoets_gebruiker_vraag_antwoord.status_voortgang
                FROM eindtoets_gebruiker_vraag_antwoord
                WHERE eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = :nascholingsartikel_id
                GROUP BY eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id, poging
            ) as voortgangen
            GROUP BY status_voortgang
        ";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['nascholingsartikel_id' => $nascholingsartikel->id]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getStatussenLaatstePogingenNascholingsartikel(Nascholingsartikel $nascholingsartikel){
        $sql = "
            SELECT voortgangen.status_voortgang, COUNT(*) as aantal
            FROM (
                SELECT status_voortgang
                FROM eindtoets_gebruiker_vraag_antwoord
                JOIN (
                    SELECT nascholingsartikel_id, front_gebruiker_id, MAX(poging) as max_poging
                    FROM eindtoets_gebruiker_vraag_antwoord
                    WHERE eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = :nascholingsartikel_id
                    GROUP BY front_gebruiker_id
                ) as voortgangen ON eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id = voortgangen.front_gebruiker_id AND eindtoets_gebruiker_vraag_antwoord.poging = voortgangen.max_poging AND eindtoets_gebruiker_vraag_antwoord.nascholingsartikel_id = voortgangen.nascholingsartikel_id 
                GROUP BY eindtoets_gebruiker_vraag_antwoord.status_voortgang, eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id
            ) as voortgangen
            GROUP BY status_voortgang
        ";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['nascholingsartikel_id' => $nascholingsartikel->id]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}