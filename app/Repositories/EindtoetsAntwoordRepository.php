<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 11:25
 */

namespace app\Repositories;

use App\Models\EindtoetsAntwoord;
use Illuminate\Database\Capsule\Manager;

class EindtoetsAntwoordRepository extends BaseRepository{

    public $reporting;

    public function __construct(Manager $manager){
        $this->reporting = $manager->getConnection('elearning')->getPdo();
    }

    public function getAantalLaatstePoging(EindtoetsAntwoord $eindtoetsAntwoord): int{
        $sql = "
            SELECT COUNT(*) as aantal FROM (
                SELECT MAX(poging) FROM eindtoets_gebruiker_vraag_antwoord
                WHERE eindtoets_antwoord_id = :eindtoets_antwoord_id
                GROUP BY eindtoets_gebruiker_vraag_antwoord.front_gebruiker_id
            ) as max_pogingen;
        ";

        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['eindtoets_antwoord_id' => $eindtoetsAntwoord->id]);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data['aantal'];
    }

    public function getAantalAllePogingen(EindtoetsAntwoord $eindtoetsAntwoord): int{
        $sql = "
            SELECT COUNT(*) as aantal FROM eindtoets_gebruiker_vraag_antwoord
            WHERE eindtoets_antwoord_id = :eindtoets_antwoord_id;
        ";

        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(['eindtoets_antwoord_id' => $eindtoetsAntwoord->id]);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data['aantal'];
    }
}