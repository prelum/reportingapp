<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;
use App\Models\Product;
/**
 * Description of AbonnementenRepository
 *
 * @author thomas
 */
class GoogleAnalyticsRepository extends BaseRepository {

    protected $googleAnalyticsClient;

    public function __construct(\Google_Service_Analytics $googleAnalyticsClient) {
        $this->googleAnalyticsClient = $googleAnalyticsClient;
    }

    public function getPageViews(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:pageviews', array('dimensions' => 'ga:date'));
        $data = array();
        foreach ($result->rows as $row) {
            $data[] = (int) $row[1];
        }
        return $data;
    }

    public function getCountries(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:sessions', array('dimensions' => 'ga:country', 'sort' => '-ga:sessions', 'max-results' => 6));
        $data = array();
        foreach ($result->rows as $key => $aantal) {
            $data[$key]['name'] = $aantal[0];
            $data[$key]['y'] = (int) $aantal[1];
        }
        return $data;
    }

    public function getBrowsers(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:sessions', array('dimensions' => 'ga:browser', 'sort' => '-ga:sessions', 'max-results' => 6));
        $data = array();
        foreach ($result->rows as $key => $aantal) {
            $data[$key]['name'] = $aantal[0];
            $data[$key]['y'] = (int) $aantal[1];
        }
        return $data;
    }

    public function getUniquePageViews(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:uniquePageviews', array('dimensions' => 'ga:pageTitle', 'sort' => '-ga:uniquePageviews', 'max-results' => 100));
        return $result->rows;
    }

    public function getNavigationFrom(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:uniquePageviews', array('dimensions' => 'ga:source', 'sort' => '-ga:uniquePageviews', 'max-results' => 100));
        return $result->rows;
    }

    public function getPopulaireZoekwoorden(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:uniquePageviews', array('dimensions' => 'ga:keyword', 'sort' => '-ga:uniquePageviews', 'max-results' => 100));
        return $result->rows;
    }

    public function getUniqueUsers(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:newUsers');
        return $result->rows[0][0];
    }

    public function getAverageSessionDuration(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:avgSessionDuration');
        return $result->rows[0][0];
    }

    public function getSingleVisitPercentage(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:bounceRate');
        return $result->rows[0][0];
    }

    public function getNewSessionsPercentage(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:percentNewSessions');
        return $result->rows[0][0];
    }

    public function getAverageSessionsPerUser(Product $product, $van, $tot) {
        $result = $this->googleAnalyticsClient->data_ga->get('ga:' . $product->GAcode, $van, $tot, 'ga:sessionsPerUser');
        return $result->rows[0][0];
    }

}
