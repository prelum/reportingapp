<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:46
 */

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Database\Capsule\Manager;

class DagtellingRepository extends BaseRepository{
    private $reporting;

    public function __construct(Manager $manager){
        $this->reporting = $manager->getConnection('reporting')->getPdo();
    }

    public function countActiveSubscriptions(\DateTime $date, Product $product = null){
        if(!empty($product)){
            $productFilter = "AND datacode = '$product->datacode'";
        }

        $sql = "INSERT INTO data_abonnementen_actief (datum, datacode, aAbosoort, aKorting, aColabo, aantal)
                SELECT :datum as datum, datacode, aAbosoort, aKorting, aColabo, COUNT(*) as aantal FROM abonnementen
                WHERE status = 'actief'
                $productFilter
                AND aBegindatum <= :begindatum
                AND (aEinddatum > :einddatum OR aEinddatum IS NULL OR aEinddatum ='0000-00-00')
                GROUP by datacode, aAbosoort, aKorting, aColabo;
                ";

        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(array('datum' => $date->format('Y-m-d'), 'begindatum' => $date->format('Y-m-d'), 'einddatum' => $date->format('Y-m-d')));
    }

    public function countRegistrations(\DateTime $date, Product $product = null){
        if(!empty($product)){
            $productFilter = "AND datacode = '$product->datacode'";
        }
        $sql = "INSERT INTO data_abonnementen_registraties (datum, datacode, aAbosoort, aKorting, aColabo, aantal)
                SELECT :datum as datum, datacode, aAbosoort, aKorting, aColabo, COUNT(*) as aantal FROM abonnementen
                WHERE status = 'actief'
                $productFilter
                AND insertDate = :insertdatum
                GROUP by datacode, aAbosoort, aKorting, aColabo;";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(array('datum' => $date->format('Y-m-d'), 'insertdatum' => $date->format('Y-m-d')));
    }

    public function countSubscriptionEnds(\DateTime $date, Product $product = null){
        if(!empty($product)){
            $productFilter = "AND datacode = '$product->datacode'";
        }
        $sql = "INSERT INTO data_abonnementen_eindes (datum, datacode, aAbosoort, aKorting, aColabo, aantal)
                SELECT :datum as datum, datacode, aAbosoort, aKorting, aColabo, COUNT(*) as aantal FROM abonnementen
                WHERE status = 'actief'
                $productFilter
                AND aEinddatum = :einddatum
                GROUP by datacode, aAbosoort, aKorting, aColabo;";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(array('datum' => $date->format('Y-m-d'), 'einddatum' => $date->format('Y-m-d')));
    }

    public function countSubscriptionStarts(\DateTime $date, Product $product = null){
        if(!empty($product)){
            $productFilter = "AND datacode = '$product->datacode'";
        }
        $sql = "INSERT INTO data_abonnementen_starts (datum, datacode, aAbosoort, aKorting, aColabo, aantal)
                SELECT :datum as datum, datacode, aAbosoort, aKorting, aColabo, COUNT(*) as aantal FROM abonnementen
                WHERE status = 'actief'
                $productFilter
                AND aBegindatum = :begindatum
                GROUP by datacode, aAbosoort, aKorting, aColabo;";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute(array('datum' => $date->format('Y-m-d'), 'begindatum' => $date->format('Y-m-d')));
    }
}