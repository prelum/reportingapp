<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Database\Capsule\Manager;

/**
 * Description of DemografieRepository
 *
 * @author thomas
 */
class ImportRepository extends BaseRepository{

    private $reporting;
    private $elearning;
    private $main;

    public function __construct(Manager $manager){
        $this->reporting = $manager->getConnection('reporting')->getPdo();
        $this->elearning = $manager->getConnection('elearning')->getPdo();
        $this->main = $manager->getConnection('main')->getPdo();
    }

    public function getElearningMutaties(\DateTime $date = null, array $tables = [], $page = 0, $limit = 100){
        $clauses = [];
        if(!empty($date)){
            $date = $date->format('Y-m-d');
            $clauses[] = "DATE(created_at) = DATE('$date')";
        }

        if(!empty($tables)){
            $clauses[] = "table_name IN ('" . implode("','", $tables) . "')";
        }

        $counter = 0;
        $clause = array_reduce($clauses, function($carry, $item) use ($counter){
            if(empty($item)){
                return $carry;
            }

            if($counter == 0){
                return "WHERE $item";
            }
            $counter++;
            if($counter > 0){
                return "AND $item";
            }
        }, '');

        $offset = $page * $limit;

        $sql = "
          SELECT * FROM dump_mutations 
          $clause 
          LIMIT :limit OFFSET :offset;
        ";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['limit' => $limit, 'offset' => $offset]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMainUserMutaties(\DateTime $date = null, $page = 0, $limit = 100){
        $date_clause = '';
        if(!empty($date)){
            $date = $date->format('Y-m-d');
            $date_clause = "WHERE DATE(mainMutatiesGegevens.insertDatum) = DATE('$date')";
        }

        $offset = $page * $limit;

        $sql = "
            SELECT mainUsrinfo.* FROM mainMutatiesGegevens
            JOIN mainUsrinfo ON mainMutatiesGegevens.usrinfoID = mainUsrinfo.id
            $date_clause
            LIMIT :limit OFFSET :offset;
        ";
        $stmt = $this->main->prepare($sql);
        $stmt->execute(['limit' => $limit, 'offset' => $offset]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function retrieveRegistrations(){
        $sql = "
                SELECT
                    IF(mainUsrinfo.productCode = 'ACC' OR mainUsrinfo.productCode = 'QP',CONCAT(mainUsrinfo.productCode, vBeroepsver), mainUsrinfo.productCode) as datacode,
                    mainUsrinfo.productCode,
                    inschrijvingID,
                    datum,
                    abonnementStart,
                    abonnementType,
                    abonnementPrijs,
                    introductieKorting,
                    kortingsCode,
                    beroepsvereniging,
                    verstuurKennismelding,
                    verstuurNieuwsbrief,
                    typeProefnummer
                FROM mainInschrijvingen
            ;";
        $stmt = $this->reporting->prepare($sql);
        $stmt->execute();
    }

    public function getMainuser($id){
        $sql = "      
                SELECT 
                mainUsrinfo.id as mainUsrinfo_id
                , IF(mainUsrinfo.productCode = 'ACC' OR mainUsrinfo.productCode = 'QP' OR mainUsrinfo.productCode = 'NA' OR mainUsrinfo.productCode = 'NAGGZ' OR mainUsrinfo.productCode = 'NAOZ',CONCAT(mainUsrinfo.productCode, vBeroepsver), mainUsrinfo.productCode) as datacode
                , mainUsrinfo.productCode
                , userid
                , inschrijving
                , insertDate
                , lastChangeDate
                , status
                , IF(freeList IS NULL, aAbosoort, CONCAT(aAbosoort, ' - ',freeList)) as aAbosoort
                , IF(aKorting = '000' OR aKorting = '0' OR aKorting = '', '000', aKorting) as aKorting
                , IFNULL(aColabo,'') as aColabo
                , aBegindatum
                , IF(aEinddatum = '0000-00-00', NULL, aEinddatum) as aEinddatum
                , vAanhef
                , vVoorletters
                , vTussenvoegsels
                , vNaam
                , vAfdeling
                , vOrganisatie
                , vAdres
                , vNummer
                , vToevoeging
                , vPostcodeNum
                , vPostcodeChar
                , vPostcodeBuitenland
                , vWoonplaats
                , vLand
                , IF(vGeboortedatum = '0000-00-00', NULL, vGeboortedatum) as vGeboortedatum
                , vEmail
                , IF(mainUsrinfo.productCode = 'ACC' OR mainUsrinfo.productCode = 'QP' OR mainUsrinfo.productCode = 'NA' OR mainUsrinfo.productCode = 'NAGGZ' OR mainUsrinfo.productCode = 'NAOZ','', vBeroepsver) as vBeroepsver
                FROM mainUsrinfo
                JOIN mainAboinfo on mainUsrinfo.`orderref` = mainAboinfo.`orderref` and mainUsrinfo.`productCode` = mainAboinfo.`productCode`
                JOIN mainSendinfo on mainUsrinfo.`sendref` = mainSendinfo.`sendref` and mainUsrinfo.`productCode` = mainSendinfo.`productCode`
                LEFT JOIN (
					 	SELECT * FROM mainRelatieEenopMeer
					 	WHERE productCode = 'PSF'
					 	AND todo = 'soortKlant'
					 	UNION 
					 	SELECT * FROM mainRelatieEenopEen
				 		WHERE (productCode = 'QP' AND todo = 'aboSoort')
				 		OR ((productCode = 'NA' OR productCode = 'NAGGZ' OR productCode = 'NAOZ') AND todo = 'aboSoort' AND freeList <> 'none')
					 ) as relatie ON relatie.overigeref = mainUsrinfo.overigeref and mainUsrinfo.`productCode` = relatie.`productCode`
                WHERE mainUsrinfo.id = :id;
            ";
        $stmt = $this->main->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getUitgave($id){
        $sql = "SELECT * FROM uitgave where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNascholingsartikel($id){
        $sql = "SELECT * FROM nascholingsartikel where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getUitgaveNascholingsartikelen($uitgave_id){
        $sql = "SELECT id FROM nascholingsartikel where uitgave_id = :uitgave_id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['uitgave_id' => $uitgave_id]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAuteur($id){
        $sql = "SELECT * FROM auteur where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getEindtoetsAntwoord($id){
        $sql = "SELECT * FROM eindtoets_antwoord where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getEindtoetsVraag($id){
        $sql = "SELECT * FROM eindtoets_vraag where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNascholingsartikelVoortgang($id){
        $sql = "SELECT * FROM nascholingsartikel_voortgang where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getUitgaveVoortgang($id){
        $sql = "SELECT * FROM uitgave_voortgang where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getFrontGebruiker($id){
        $sql = "SELECT * FROM front_gebruiker where id = :id";
        $stmt = $this->elearning->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

}
