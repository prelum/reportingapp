<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 13:28
 */

namespace App\Traits;

/**
 * Trait Cleans
 * @package App\Traits
 *
 * Trait die methode geeft om een array aan data schoon te maken met variabele cleaners.
 *
 * Beschikbare cleaners zijn te vinden onder Services/Cleaner/Cleaners
 *
 */
trait Cleans{

    public function clean($data, array $settings = []){
        foreach($settings as $field => $cleaners){
            foreach($cleaners as $cleaner){
                $data[$field] = $cleaner::clean($data[$field]);
            }
        }
        return $data;
    }
}