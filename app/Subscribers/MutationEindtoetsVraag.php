<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 14:42
 */

namespace App\Subscribers;


use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\EindtoetsVraag;
use App\Models\Mutatie;
use App\Repositories\ImportRepository;

class MutationEindtoetsVraag extends AbstractSubscriber{

    protected $importRepository;

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            ['elearning.eindtoets_vraag.insert', 'elearning.eindtoets_vraag.update'],
            self::class . '@updateInsert'
        );
    }

    public function updateInsert(Mutatie $mutatie){
        $data = $this->importRepository->getEindtoetsVraag($mutatie->table_record_id);
        EindtoetsVraag::updateOrCreate(
            ['id' => $data['id']],
            $data
        );
    }
}