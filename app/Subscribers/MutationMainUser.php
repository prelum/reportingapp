<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 12:09
 */

namespace App\Subscribers;


use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\Abonnement;
use App\Models\Mutatie;
use App\Repositories\ImportRepository;
use App\Services\Cleaner\Cleaners\DateCleaner;
use App\Services\Cleaner\Cleaners\TrimCleaner;
use App\Traits\Cleans;

class MutationMainUser extends AbstractSubscriber{

    use Cleans;

    protected $importRepository;

    protected $mainuser_settings = [
        'datacode' => [
            TrimCleaner::class
        ],
        'insertDate' => [
            DateCleaner::class
        ],
        'lastChangeDate' => [
            DateCleaner::class
        ],
        'aBegindatum' => [
            DateCleaner::class
        ],
        'aEinddatum' => [
            DateCleaner::class
        ],
        'vGeboortedatum' => [
            DateCleaner::class
        ],
    ];

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            'main.mainUsrinfo.composite',
            self::class . '@updateInsert'
        );
    }

    public function updateInsert(Mutatie $mutatie){
        $users = $this->importRepository->getMainuser($mutatie->table_record_id);

        foreach($users as $user){
            $data = $this->clean($user, $this->mainuser_settings);
            Abonnement::updateOrCreate(
                ['mainUsrinfo_id' => $data['mainUsrinfo_id']],
                $data
            );
        }

    }
}