<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 16:00
 */

namespace App\Subscribers;

use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class RecalculateNascholingsartikel extends AbstractSubscriber{

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen('recalculate.nascholingsartikel', self::class . '@recalculate');
    }

    public function recalculate($id){
        $console = $this->container->get(Application::class);
        $input = new ArrayInput(array(
            'command' => 'analyseren:nascholingsartikel',
            'nascholingsartikel-id' => $id,
        ));
        $output = new NullOutput();
        $console->run($input, $output);
    }
}