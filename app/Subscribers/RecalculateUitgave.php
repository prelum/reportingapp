<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 16:00
 */

namespace App\Subscribers;

use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;

class RecalculateUitgave extends AbstractSubscriber{

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen('recalculate.uitgave', self::class . '@recalculate');
    }

    public function recalculate($id){
        $console = $this->container->get(Application::class);
        $input = new ArrayInput(array(
            'command' => 'analyseren:uitgave',
            'uitgave-id' => $id,
        ));
        $output = new NullOutput();
        $console->run($input, $output);
    }
}