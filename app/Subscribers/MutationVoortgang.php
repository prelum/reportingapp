<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13-2-2017
 * Time: 15:26
 */

namespace App\Subscribers;


use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\Abonnement;
use App\Models\Mutatie;
use App\Models\Nascholingsartikel;
use App\Models\Product;
use App\Models\Uitgave;
use App\Models\Voortgang;
use App\Repositories\ImportRepository;

class MutationVoortgang extends AbstractSubscriber{

    public $importRepository;

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            [
                'elearning.nascholingsartikel_voortgang.insert',
                'elearning.nascholingsartikel_voortgang.update',
            ],
            self::class . '@updateInsertNascholingsArtikelVoortgang'
        );
        $dispatcher->listen(
            [
                'elearning.uitgavetoets_voortgang.insert',
                'elearning.uitgavetoets_voortgang.update',
            ],
            self::class . '@updateInsertUitgaveVoortgang'
        );

    }

    public function updateInsertNascholingsArtikelVoortgang(Mutatie $mutatie){
        $data = $this->importRepository->getNascholingsartikelVoortgang($mutatie->table_record_id);

        if(empty($data)){
            return;
        }
        $frontGebruiker = $this->importRepository->getFrontGebruiker($data['front_gebruiker_id']);
        $product = Product::where('swis_id', $frontGebruiker['branding_id'])->first();

        if(empty($frontGebruiker) || empty($product)){
            return;
        }

        $abonnement = Abonnement::where('userid', $frontGebruiker['prelum_id'])->where('productCode', $product->productCode)->first();
        $nascholingsartikel = Nascholingsartikel::find($data['nascholingsartikel_id']);

        Voortgang::updateOrCreate(
            [
                'abonnement_id' => (empty($abonnement)) ? null : $abonnement->id,
                'nascholingsartikel_id' => (empty($nascholingsartikel)) ? null : $nascholingsartikel->id,
            ],
            [
                'abonnement_id' => (empty($abonnement)) ? null : $abonnement->id,
                'uitgave_id' => (empty($nascholingsartikel) || empty($nascholingsartikel->uitgave)) ? null : $nascholingsartikel->uitgave->id,
                'nascholingsartikel_id' => (empty($nascholingsartikel)) ? null : $nascholingsartikel->id,
                'voortgang_type' => ($data['uitgave_voortgang_id'] == 0) ? 'artikel' : 'uitgave',
                'status' => $data['status'],
                'datum' => $data['datum']
            ]
        );
    }

    public function updateInsertUitgaveVoortgang(Mutatie $mutatie){
        $data = $this->importRepository->getUitgaveVoortgang($mutatie->table_record_id);

        if(empty($data)){
            return;
        }

        $uitgave = Uitgave::find($data['uitgave_id']);
        $frontGebruiker = $this->importRepository->getFrontGebruiker($data['front_gebruiker_id']);

        if(empty($frontGebruiker) || empty($product)){
            return;
        }
        $product = Product::where('swis_id', $frontGebruiker['branding_id'])->first();
        $abonnement = Abonnement::where('userid', $frontGebruiker['prelum_id'])->where('productCode', $product->productCode)->first();

        Voortgang::updateOrCreate(
            [
                'abonnement_id' => (empty($abonnement)) ? null : $abonnement->id,
                'uitgave_id' => (empty($uitgave)) ? null : $uitgave->id,
            ],
            [
                'abonnement_id' => (empty($abonnement)) ? null : $abonnement->id,
                'uitgave_id' => (empty($uitgave)) ? null : $uitgave->id,
                'voortgang_type' => 'uitgave_los',
                'status' => $data['status'],
                'datum' => $data['datum']
            ]
        );
    }
}