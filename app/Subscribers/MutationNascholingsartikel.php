<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 11:51
 */

namespace App\Subscribers;

use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\Auteur;
use App\Models\Mutatie;
use App\Models\Nascholingsartikel;
use App\Models\Product;
use App\Repositories\ImportRepository;
use App\Services\Cleaner\Cleaners\DateCleaner;
use App\Traits\Cleans;

class MutationNascholingsartikel extends AbstractSubscriber{

    use Cleans;

    protected $importRepository;
    protected $nascholingsartikel_settings = [
        'ahead_of_publication_date' => [
            DateCleaner::class
        ],
        'publicatie_datum' => [
            DateCleaner::class
        ],
        'eind_datum' => [
            DateCleaner::class
        ],
        'rubriek_id' => []
    ];
    protected $auteur_settings = [

    ];

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            ['elearning.nascholingsartikel.insert', 'elearning.nascholingsartikel.update'],
            self::class . '@updateInsert'
        );
    }

    public function updateInsert(Mutatie $mutatie){
        $data = $this->importRepository->getNascholingsartikel($mutatie->table_record_id);

        $producten = Product::where('swis_id', $data['branding_id'])->get();

        if($producten->count() == 1){
            $data['product_id'] = $producten[0]->id;
        }else{
            $data['product_id'] = null;
        }

        $data = $this->clean($data, $this->nascholingsartikel_settings);

        $nascholingsartikel = Nascholingsartikel::updateOrCreate(
            ['id' => $data['id']],
            $data
        );
        $auteur_ids = !isset($data['auteurs']) ? [] : explode(',', $data['auteurs']);
        $this->addAuteurs($nascholingsartikel, $auteur_ids);
    }

    protected function addAuteurs(Nascholingsartikel $nascholingsartikel, array $auteur_ids){
        $nascholingsartikel->auteurs()->detach();


        foreach($auteur_ids as $key => $auteur_id){
            $data = $this->importRepository->getAuteur((int)$auteur_id);
            if(empty($data)){
                return;
            }
            $data['product_id'] = $nascholingsartikel->product_id;
            $data = $this->clean($data, $this->auteur_settings);
            Auteur::updateOrCreate(
                ['id' => $data['id']],
                $data
            );
            $nascholingsartikel->auteurs()->attach($data['id'], ['volgorde' => $key + 1]);
        }
        $nascholingsartikel->save();
    }
}