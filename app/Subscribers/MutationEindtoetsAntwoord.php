<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2017
 * Time: 14:42
 */

namespace App\Subscribers;


use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\EindtoetsAntwoord;
use App\Models\Mutatie;
use App\Repositories\ImportRepository;
use App\Services\Cleaner\Cleaners\ConverteerGoedFoutCleaner;
use App\Traits\Cleans;

class MutationEindtoetsAntwoord extends AbstractSubscriber{

    use Cleans;

    protected $importRepository;

    protected $antwoord_settings = [
        'goed_fout' => [
            ConverteerGoedFoutCleaner::class
        ]
    ];

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            ['elearning.eindtoets_antwoord.insert', 'elearning.eindtoets_antwoord.update'],
            self::class . '@updateInsert'
        );
    }

    public function updateInsert(Mutatie $mutatie){
        $data = $this->importRepository->getEindtoetsAntwoord($mutatie->table_record_id);
        $data = $this->clean($data, $this->antwoord_settings);
        EindtoetsAntwoord::updateOrCreate(
            ['id' => $data['id']],
            $data
        );
    }
}