<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 31-1-2017
 * Time: 16:39
 */

namespace App\Subscribers;


use App\Core\AbstractSubscriber;
use App\Core\Dispatcher;
use App\Models\Mutatie;
use App\Models\Product;
use App\Models\Uitgave;
use App\Repositories\ImportRepository;
use App\Services\Cleaner\Cleaners\DateCleaner;
use App\Traits\Cleans;

class MutationUitgave extends AbstractSubscriber {

    use Cleans;

    protected $importRepository;
    protected $uitgave_settings = [
        'publicatie_datum' => [
            DateCleaner::class
        ],
        'eind_datum' => [
            DateCleaner::class
        ]
    ];

    public function __construct(ImportRepository $importRepository){
        $this->importRepository = $importRepository;
    }

    public function subscribe(Dispatcher $dispatcher){
        $dispatcher->listen(
            ['elearning.uitgave.insert', 'elearning.uitgave.update'],
            self::class . '@updateInsert'
        );
    }

    public function updateInsert(Mutatie $mutatie){
        $uitgave = $this->importRepository->getUitgave($mutatie->table_record_id);
        $data = $this->clean($uitgave, $this->uitgave_settings);

        $producten = Product::where('swis_id', $data['branding_id'])->get();

        if($producten->count() == 1){
            $data['product_id'] = $producten[0]->id;
        }else{
            $data['product_id'] = null;
        }

        Uitgave::updateOrCreate(
            ['id' => $data['id']],
            $data
        );
    }
}