# Prelum ReportingApp #

## Installatie en deployment ##

### Ontwikkeling ###

Clone de repo en kopieer vervolgens .env.example en hernoem deze naar .env.

Er is een VagrantBox aanwezig om een dummy-versie van de reportingapp te installeren. Deze is gebaseerd op Laravel Homestead, dus er moet eerst een composer installatie plaats vinden voordat deze opgehaald kan worden. De default waardes in .env.example worden ook gebruikt om de box op te zetten

Doe vervolgens:
```
vagrant up
```

SSH de box binnen, ga naar de root dir van het project en voer de volgende actie uit:
```
vendor/bin/phinx migrate
```
Hiermee zal in de gespecificieerde database (default v127_reporting) de database structuur weggeschreven worden.

Hierna kunnen ook de web resources opgehaald worden:
```
npm install
```

De opgehaalde resources kunnen vervolgens gecompileerd worden doormiddel van een gulp-task:
```
gulp
```

Er zijn verder geen seeders voor de data meegeleverd. Het is dus aan te raden een export te maken van de acceptatie databases en deze in te lezen om gegevens hebben waarmee gewerkt kan worden.

Let er tot slot op dat toegang tot de applicatie gelimiteerd is op een whitelist aan ip's in de .htaccess file. Voor development moet je hier even het IP-adres van je PC voor vagrant aan toe voegen, anders zal het systeem zeggen dat je geen toegangsrechten hebt.

### Updaten ###
Het updaten van de applicatie is simpel te doen door onderstaande acties uit te voeren nadat de code op de server is gepuld uit de repo.
```
composer update             # updaten van PHP libs
vendor/bin/phinx migrate    # updaten van de database
```
[DeployHQ](https://dvk-media-group.deployhq.com/projects/reportingapp/overview) voert deze acties nu automatisch uit bij elke deploy.

## Packages ##

Zie de composer.json en package.json voor alle PHP en JS libraries die in dit project gebruikt worden. Neem ook even de documentatie hiervan door om te zien welke mogelijkheden de packages bieden.

## Overig ##
- Zie de documentatie van [dataverwerking](docs/dataverwerking.md) voor meer informatie over de manier waarop data verwerkt word in de applicatie, en in welke staat deze momenteel verkeerd.
