<?php

use Phinx\Migration\AbstractMigration;

class CreateProductenTable extends AbstractMigration
{
    public function up(){
        $table = $this->table('producten');
        $table->addColumn('zendesk_id', 'integer');
        $table->addColumn('swis_id', 'integer');
        $table->addColumn('GAcode', 'integer');
        $table->addColumn('naam', 'string');
        $table->addColumn('eigenaar', 'string');
        $table->addColumn('start_jaar', 'date');
        $table->addColumn('productCode', 'string');
        $table->addColumn('GAcode', 'string');
        $table->addColumn('heeft_abonnementen', 'boolean');
        $table->addColumn('heeft_koop', 'boolean');
        $table->addColumn('heeft_elearning', 'boolean');
        $table->addColumn('heeft_events', 'boolean');
        $table->addColumn('heeft_marketing', 'boolean');
        $table->addColumn('heeft_proefnummers', 'boolean');
        $table->addColumn('heeft_analytics', 'boolean');
        $table->addColumn('website_url', 'string');
        $table->addColumn('supergroep', 'string');
        $table->save();
    }

    public function down(){
        $this->table('producten')->drop();
    }
}
