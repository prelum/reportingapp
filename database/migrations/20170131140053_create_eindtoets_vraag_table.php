<?php

use Phinx\Migration\AbstractMigration;

class CreateEindtoetsVraagTable extends AbstractMigration{
    public function up(){
        $table = $this->table('eindtoets_vragen');
        $table->addColumn('swis_id', 'integer', array('null' => true));
        $table->addColumn('nascholingsartikel_id', 'integer', array('null' => true));
        $table->addColumn('antwoorden_template_id', 'integer', array('null' => true));
        $table->addColumn('type_vraag', 'string', array('null' => true));
        $table->addColumn('vraag', 'text', array('null' => true));
        $table->addColumn('juiste_antwoord', 'text', array('null' => true));
        $table->addColumn('feedback', 'text', array('null' => true));
        $table->addColumn('feedback_verdieping_titel', 'text', array('null' => true));
        $table->addColumn('feedback_verdieping', 'text', array('null' => true));
        $table->addColumn('volgorde', 'float', array('null' => true));
        $table->addColumn('willekeurig_sorteren', 'integer', array('null' => true));
        $table->addColumn('video', 'string', array('null' => true));
        $table->addColumn('video_id', 'string', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('eindtoets_vragen')->drop();
    }
}
