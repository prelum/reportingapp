<?php

use Phinx\Migration\AbstractMigration;

class CreateEindtoetsAntwoordTable extends AbstractMigration
{
    public function up(){
        $table = $this->table('eindtoets_antwoorden');
        $table->addColumn('swis_id', 'integer', array('null' => true));
        $table->addColumn('vraag_id', 'integer', array('null' => true));
        $table->addColumn('antwoord', 'text', array('null' => true));
        $table->addColumn('goed_fout', 'string', array('null' => true));
        $table->addColumn('volgorde', 'float', array('null' => true));
        $table->addColumn('alle_pogingen', 'integer', array('null' => true));
        $table->addColumn('laatste_pogingen', 'integer', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        //$this->table('eindtoets_antwoorden')->drop();
    }
}
