<?php

use Phinx\Migration\AbstractMigration;

class CreateUitgavesTable extends AbstractMigration{
    public function up(){
        $table = $this->table('uitgaves');
        $table->addColumn('swis_id', 'integer', array('null' => true));
        $table->addColumn('product_id', 'integer', array('null' => true));
        $table->addColumn('branding_id', 'integer', array('null' => true));
        $table->addColumn('titel', 'string', array('null' => true));
        $table->addColumn('jaar', 'integer', array('null' => true));
        $table->addColumn('jaargang', 'integer', array('null' => true));
        $table->addColumn('vereiste_percentage', 'integer', array('null' => true));
        $table->addColumn('aantal_pogingen', 'integer', array('null' => true));
        $table->addColumn('publicatie_datum', 'date', array('null' => true));
        $table->addColumn('eind_datum', 'date', array('null' => true));
        $table->addColumn('accreditatiepunt', 'integer', array('null' => true));
        $table->addColumn('unieke_gebruikers', 'integer', array('null' => true));
        $table->addColumn('gemiddelde_score', 'integer', array('null' => true));
        $table->addColumn('gemiddelde_score_laatste_poging', 'integer', array('null' => true));
        $table->addColumn('gemiddelde_aantal_pogingen', 'integer', array('null' => true));
        $table->addColumn('statussen_voortgang', 'text', array('null' => true));
        $table->addColumn('statussen_voortgang_laatste_poging', 'text', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('uitgaves')->drop();
    }
}
