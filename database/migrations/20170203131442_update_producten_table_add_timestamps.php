<?php

use Phinx\Migration\AbstractMigration;

class UpdateProductenTableAddTimestamps extends AbstractMigration{
    public function up(){
        $table = $this->table('producten');
        $table->addColumn('type_voortgang', 'string', ['default' => '', 'after' => 'naam']);
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $table = $this->table('producten');
        $table->removeColumn('type_voortgang');
        $table->removeColumn('created_at');
        $table->removeColumn('updated_at');
    }
}
