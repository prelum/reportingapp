<?php

use Phinx\Migration\AbstractMigration;

class CreateNascholingsartikelenTable extends AbstractMigration{
    public function up(){
        $table = $this->table('nascholingsartikelen');
        $table->addColumn('swis_id', 'integer', array('null' => true));
        $table->addColumn('product_id', 'integer', array('null' => true));
        $table->addColumn('branding_id', 'integer', array('null' => true));
        $table->addColumn('uitgave_id', 'integer', array('null' => true));
        $table->addColumn('rubriek_id', 'string', array('default' => ''));
        $table->addColumn('titel', 'string', array('null' => true));
        $table->addColumn('ondertitel', 'string', array('null' => true));
        $table->addColumn('samenvatting', 'text', array('null' => true));
        $table->addColumn('type', 'string', array('null' => true));
        $table->addColumn('vereiste_percentage', 'integer', array('null' => true));
        $table->addColumn('aantal_pogingen', 'integer', array('null' => true));
        $table->addColumn('publicatie_datum', 'date', array('null' => true));
        $table->addColumn('eind_datum', 'date', array('null' => true));
        $table->addColumn('ahead_of_publication_date', 'date', array('null' => true));
        $table->addColumn('online', 'boolean', array('null' => true));
        $table->addColumn('gaia_cursus_id', 'string', array('null' => true));
        $table->addColumn('gaia_module_id', 'string', array('null' => true));
        $table->addColumn('volgorde', 'integer', array('null' => true));
        $table->addColumn('accreditatiepunt', 'integer', array('null' => true));
        $table->addColumn('beoordeeld', 'boolean', array('null' => true));
        $table->addColumn('header_afbeelding', 'string', array('null' => true));
        $table->addColumn('unieke_gebruikers', 'integer', array('null' => true));
        $table->addColumn('gemiddelde_score', 'float', array('null' => true));
        $table->addColumn('gemiddelde_score_laatste_poging', 'float', array('null' => true));
        $table->addColumn('gemiddelde_aantal_pogingen', 'float', array('null' => true));
        $table->addColumn('statussen_voortgang', 'text', array('null' => true));
        $table->addColumn('statussen_voortgang_laatste_poging', 'text', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('nascholingsartikelen')->drop();
    }
}
