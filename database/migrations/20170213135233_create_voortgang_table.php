<?php

use Phinx\Migration\AbstractMigration;

class CreateVoortgangTable extends AbstractMigration{
    public function up(){
        $table = $this->table('voortgangen');
        $table->addColumn('abonnement_id', 'integer', ['null' => true]);
        $table->addColumn('uitgave_id', 'integer', ['null' => true]);
        $table->addColumn('nascholingsartikel_id', 'integer', ['null' => true]);
        $table->addColumn('voortgang_type', 'string', ['null' => true]);
        $table->addColumn('status', 'string', ['null' => true]);
        $table->addColumn('datum', 'datetime', ['null' => true]);
        $table->addTimestamps();
        $table->addIndex('abonnement_id');
        $table->addIndex('uitgave_id');
        $table->addIndex('nascholingsartikel_id');
        $table->addIndex(['abonnement_id', 'uitgave_id', 'nascholingsartikel_id'], array('unique' => true));
        $table->save();
    }

    public function down(){
        $this->table('voortgangen')->drop();
    }
}
