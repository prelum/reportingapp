<?php

use Phinx\Migration\AbstractMigration;

class CreateMutationsTable extends AbstractMigration{
    public function up(){
        $table = $this->table('mutaties');
        $table->addColumn('connection', 'string');
        $table->addColumn('table_name', 'string');
        $table->addColumn('table_record_id', 'integer');
        $table->addColumn('table_action', 'string');
        $table->addColumn('checked', 'boolean');
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('mutaties')->drop();
    }
}
