<?php

use Phinx\Migration\AbstractMigration;

class CreateAbonnementTable extends AbstractMigration{
    public function up(){
        $table = $this->table('abonnementen');
        $table->addColumn('mainUsrinfo_id', 'integer');
        $table->addColumn('datacode', 'string');
        $table->addColumn('productcode', 'string');
        $table->addColumn('userid', 'integer');
        $table->addColumn('inschrijving', 'integer', array('null' => true));
        $table->addColumn('insertDate', 'date', array('null' => true));
        $table->addColumn('lastChangeDate', 'date', array('null' => true));
        $table->addColumn('status', 'string', array('null' => true));
        $table->addColumn('aAbosoort', 'string', array('null' => true));
        $table->addColumn('aKorting', 'string', array('null' => true));
        $table->addColumn('aColabo', 'string', array('null' => true));
        $table->addColumn('aBegindatum', 'date', array('null' => true));
        $table->addColumn('aEinddatum', 'date', array('null' => true));
        $table->addColumn('vAanhef', 'string', array('null' => true));
        $table->addColumn('vVoorletters', 'string', array('null' => true));
        $table->addColumn('vTussenvoegsels', 'string', array('null' => true));
        $table->addColumn('vNaam', 'string', array('null' => true));
        $table->addColumn('vAfdeling', 'string', array('null' => true));
        $table->addColumn('vOrganisatie', 'string', array('null' => true));
        $table->addColumn('vAdres', 'string', array('null' => true));
        $table->addColumn('vNummer', 'integer', array('null' => true));
        $table->addColumn('vToevoeging', 'string', array('null' => true));
        $table->addColumn('vPostcodeNum', 'integer', array('null' => true));
        $table->addColumn('vPostcodeChar', 'string', array('null' => true));
        $table->addColumn('vPostcodeBuitenland', 'string', array('null' => true));
        $table->addColumn('vWoonplaats', 'string', array('null' => true));
        $table->addColumn('vLand', 'string', array('null' => true));
        $table->addColumn('vGeboortedatum', 'date', array('null' => true));
        $table->addColumn('vEmail', 'string', array('null' => true));
        $table->addColumn('vBeroepsver', 'string', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('abonnementen')->drop();
    }
}
