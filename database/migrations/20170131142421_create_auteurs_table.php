<?php

use Phinx\Migration\AbstractMigration;

class CreateAuteursTable extends AbstractMigration{
    public function up(){
        $table = $this->table('auteurs');
        $table->addColumn('swis_id', 'integer', array('null' => true));
        $table->addColumn('branding_id', 'integer' , array('null' => true));
        $table->addColumn('product_id', 'integer', array('null' => true));
        $table->addColumn('auteur', 'string', array('null' => true));
        $table->addColumn('titel', 'string', array('null' => true));
        $table->addColumn('beschrijving', 'text', array('null' => true));
        $table->addTimestamps();
        $table->save();
    }

    public function down(){
        $this->table('auteurs')->drop();
    }
}
