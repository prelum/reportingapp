<?php

use Phinx\Migration\AbstractMigration;

class CreateNascholingsartikelsAuteursTable extends AbstractMigration
{
    public function up(){
        $table = $this->table('nascholingsartikels_auteurs');
        $table->addColumn('nascholingsartikel_id', 'integer');
        $table->addColumn('auteur_id', 'integer');
        $table->addColumn('volgorde', 'integer');
        $table->save();
    }

    public function down(){
        $this->table('nascholingsartikels_auteurs')->drop();
    }
}
