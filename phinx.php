<?php

require __DIR__. '/bootstrap.php';

/**
 * Configuratie voor de migratie-tool
 *
 * Zie de documentatie voor meer informatie: https://phinx.org/
 */

return [
    "paths" => [
        "seeds" => "%%PHINX_CONFIG_DIR%%/database/seeds",
        "migrations" => "%%PHINX_CONFIG_DIR%%/database/migrations"
    ],
    "environments" => [
        "default_migration_table" => "migrations",
        "default_database" => "database",
        "database" => [
            "adapter" => "mysql",
            "host" => $_ENV['REPORTING_HOST'],
            "name" => $_ENV['REPORTING_DATABASE'],
            "user" => $_ENV['REPORTING_USERNAME'],
            "pass" => $_ENV['REPORTING_PASSWORD'],
            "port" => 3306,
            "charset" => "utf8"
        ]
    ]
];