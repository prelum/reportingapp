const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.styles([
        '../../../node_modules/bootstrap/dist/css/*.css',
        '../../../node_modules/font-awesome/css/font-awesome.css',
        '../../../node_modules/metismenu/dist/metisMenu.css',
        '../../../node_modules/datatables/media/css/jquery.dataTables.css',
        '../../../node_modules/highcharts/css/highcharts.css',
        '*.css'
    ], 'public/css/all.css');

    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
    mix.copy('node_modules/bootstrap/dist/fonts', 'public/fonts');
    mix.copy('node_modules/datatables/media/images', 'public/images');
    mix.copy('resources/assets/images', 'public/images');

    mix.copy('node_modules/jquery/dist/jquery.js', 'public/js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.js', 'public/js');
    mix.copy('node_modules/datatables/media/js/jquery.dataTables.js', 'public/js');
    mix.copy('node_modules/metismenu/dist/metisMenu.js', 'public/js');
    mix.copy('node_modules/highcharts/highcharts.js', 'public/js');
    mix.copy('node_modules/highcharts/modules/map.js', 'public/js/highmaps.js');
    mix.scripts([
        '*.js'
    ]);
});