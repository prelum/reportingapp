<?php

/**
 * Hier zijn de algemene settings voor de applicatie te vinden. Deze worden geladen in de container in de variabele 'settings'
 */
return [
    'httpVersion' => '1.1',
    'responseChunkSize' => 4096,
    'outputBuffering' => 'append',
    'determineRouteBeforeAppMiddleware' => false,
    'displayErrorDetails' => getenv('APP_DEBUG'),
    'addContentLengthHeader' => true,
    'routerCacheFile' => false,
    'template_dir' => '../resources/views',
    'cache_dir' => '../storage/cache',
    'database' => array(
        'main' => array(
            'driver' => 'mysql',
            'host' => getenv('MAIN_HOST'),
            'database' => getenv('MAIN_DATABASE'),
            'username' => getenv('MAIN_USERNAME'),
            'password' => getenv('MAIN_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),
        'reporting' => array(
            'driver' => 'mysql',
            'host' => getenv('REPORTING_HOST'),
            'database' => getenv('REPORTING_DATABASE'),
            'username' => getenv('REPORTING_USERNAME'),
            'password' => getenv('REPORTING_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),
        'elearning' => array(
            'driver' => 'mysql',
            'host' => getenv('ELEARNING_HOST'),
            'database' => getenv('ELEARNING_DATABASE'),
            'username' => getenv('ELEARNING_USERNAME'),
            'password' => getenv('ELEARNING_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        )
    ),
    'google' => [
        'application_name' => getenv('APP_NAME'),
        'json_key' => __DIR__ . '/keys/ReportingApp-e2adc47faf3f.json'
    ],
    'queue' => [
        [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'update',
            'retry_after' => 90,
        ],
        [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'recalculate',
            'retry_after' => 90,
        ]
    ]
];