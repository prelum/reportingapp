<?php

/**
 * Voeg hier eventuele subscribers toe voor de events-bus.
 *
 * Alle subscribers voor events worden in onderstaande volgorde afgehandeld.
 *
 * Via de subscribe-methode is het mogelijk om de subscriber naar een bepaald event te laten luisteren
 */
return [
    \App\Subscribers\MutationMainUser::class,
    \App\Subscribers\MutationNascholingsartikel::class,
    \App\Subscribers\MutationUitgave::class,
    \App\Subscribers\MutationEindtoetsAntwoord::class,
    \App\Subscribers\MutationEindtoetsVraag::class,
    \App\Subscribers\RecalculateNascholingsartikel::class,
    \App\Subscribers\MutationVoortgang::class
];