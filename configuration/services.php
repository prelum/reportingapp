<?php

/**
 * Registreer hier eventuele nieuwe services, of pas de creatie van bestaande services aan
 *
 * De services worden ingeschoten via dependency injection, gebaseerd op de PHP-DI container: http://php-di.org/.
 *
 * Autowiring staat aan, dus hier hoeven alleen de services toegevoegd te worden die afwijkende constructors hebben, of extra methods gecalled moeten hebben vóór gebruik.
 */
return [
    \Slim\Views\Twig::class => function($c){
        $twig = new \Slim\Views\Twig($c->get('settings')['template_dir'], []);

        $twig->addExtension(new \Slim\Views\TwigExtension(
            $c->get('router'),
            $c->get('request')->getUri()
        ));

        return $twig;
    },

    Google_Service_Analytics::class => function($c){
        $client = new \Google_Client();
        $client->setAuthConfig($c->get('settings')['google']['json_key']);
        $client->setApplicationName($c->get('settings')['google']['application_name']);
        $client->addScope([
            \Google_Service_Analytics::ANALYTICS
        ]);
        $GoogleAnalyticsClient = new \Google_Service_Analytics($client);
        return $GoogleAnalyticsClient;
    },

    \Illuminate\Database\Capsule\Manager::class => function(\Interop\Container\ContainerInterface $c){
        $capsule = new \Illuminate\Database\Capsule\Manager();
        foreach($c->get('settings')['database'] as $name => $config){
            $capsule->addConnection($config, $name);
        }
        return $capsule;
    },

    Symfony\Component\Console\Application::class => function(\Interop\Container\ContainerInterface $c){
        $console = new \Symfony\Component\Console\Application();
        $commands = require __DIR__ . '/commands.php';
        foreach($commands as $command){
            $console->add($c->get($command));
        }
        return $console;
    },

    Jobby\Jobby::class => function(){
        $jobby = new \Jobby\Jobby();
        $jobs = require __DIR__ . '/jobs.php';
        foreach($jobs as $name => $job){
            $jobby->add($name, $job);
        }
        return $jobby;
    },

    \App\Core\Dispatcher::class => function(\Interop\Container\ContainerInterface $c){
        $dispatcher = new \App\Core\Dispatcher($c);
        $subscribers = require __DIR__ . '/subscribers.php';

        foreach($subscribers as $subscriber){
            $dispatcher->subscribe($subscriber);
        }
        return $dispatcher;
    },

    Illuminate\Queue\Capsule\Manager::class => function(\Interop\Container\ContainerInterface $c){
        $queue = new \Illuminate\Queue\Capsule\Manager();

        foreach($c->get('settings')['queue'] as $settings){
            $queue->addConnection($settings);
        }
        return $queue;
    }
];