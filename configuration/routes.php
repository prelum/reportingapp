<?php

/**
 * Registreer hier de beschikbare routes voor de applicatie
 *
 * De Router is gebaseerd op het Slim framework. Zie de documentatie aldaar voor meer informatie: https://www.slimframework.com/.
 */

$app->get('/', [\App\Controllers\HomeController::class, "indexAction"]);
$app->get('/{productCode}/dashboard', [\App\Controllers\DashboardController::class, "indexAction"]);
$app->get('/{productCode}/abonnees/abonnementen', [\App\Controllers\AbonnementenController::class, "indexAction"]);
$app->get('/{productCode}/abonnees/demografie', [\App\Controllers\Abonnees\DemografieController::class, "indexAction"]);
$app->get('/{productCode}/abonnees/geografie', [\App\Controllers\Abonnees\GeografieController::class, "indexAction"]);
$app->get('/{productCode}/website/analytics', [\App\Controllers\Website\AnalyticsController::class, "analyticsAction"]);
$app->get('/{productCode}/proefnummers', [\App\Controllers\ProefnummersController::class, "indexAction"]);
$app->get('/{productCode}/koop', [\App\Controllers\KoopController::class, "indexAction"]);

$app->group('/{productCode}/nascholing', function(){
    $this->get('/default', [\App\Controllers\Nascholing\DefaultController::class, "indexAction"]);
    $this->group('/uitgaves', function(){
        $this->get('', [\App\Controllers\Nascholing\UitgaveController::class, "index"]);
        $this->get('/{id}', [\App\Controllers\Nascholing\UitgaveController::class, "show"]);
    });
    $this->group('/nascholingsartikelen', function(){
        $this->get('/', [\App\Controllers\Nascholing\ArtikelController::class, "index"]);
        $this->get('/{id}', [\App\Controllers\Nascholing\ArtikelController::class, "show"]);
    });
});