<?php
/**
 * Registreer hier de commands voor de applicatie. Deze zijn beschikbaar op de command line onder 'php console'
 *
 * Alle commands moeten Symfony\Component\Console\Command\Command extenden.
 *
 * Zie de documentatie daarvan voor meer informatie: http://symfony.com/doc/current/console.html.
 */
return [
    \App\Commands\Test::class,
    \App\Commands\Dagtellingen::class,
    \App\Commands\Analyseren\AnalyseNascholingsartikel::class,
    \App\Commands\Analyseren\AnalyseUitgaves::class,
    \App\Commands\Compileren\MutatiesElearning::class,
    \App\Commands\Compileren\MutatiesAbonnementen::class,
    \App\Commands\VerwerkMutaties::class,
    \App\Core\JobRunner::class
];