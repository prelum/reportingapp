<?php

/**
 * Registreer hier nieuwe cronjobs.
 *
 * Zie de documentatie van https://github.com/jobbyphp/jobby voor meer informatie over hoe een nieuwe job toe te voegen en welke mogelijkheden jobs hebben
 */
return [
    'mutaties-abonnementen' => [
        'command' => getenv('PHP_BIN') . " " . __DIR__ . "/../console compileren:mutaties-abonnementen --vanaf=yesterday --tot=today",
        'schedule' => '0 1 * * *',
        'output' => __DIR__ . '/../storage/logs/analyseren.log',
    ],
    'mutaties-elearning' => [
        'command' => getenv('PHP_BIN') . " " . __DIR__ . "/../console compileren:mutaties-elearning --vanaf=yesterday --tot=today",
        'schedule' => '0 1 * * *',
        'output' => __DIR__ . '/../storage/logs/analyseren.log',
        'enabled' => false
    ],
    'mutaties-update' => [
        'command' => getenv('PHP_BIN') . " " . __DIR__ . "/../console mutaties:update",
        'schedule' => '0 2 * * *',
        'output' => __DIR__ . '/../storage/logs/analyseren.log',
    ],
    'dagtellingen' => [
        'command' => getenv('PHP_BIN') . " " . __DIR__ . "/../console analyseren:dagtellingen --datum=yesterday",
        'schedule' => '0 3 * * *',
        'output' => __DIR__ . '/../storage/logs/analyseren.log',
    ],
    'test' => [
        'command' => getenv('PHP_BIN') . " " . __DIR__ . "/../console test",
        'schedule' => '* * * * *',
        'output' => __DIR__ . '/../storage/logs/test.log',
        'haltDir' => __DIR__ . '/../storage/logs'
    ]
];