# Dataverwerking #

## Mutaties ##

De dataverwerking wordt afgehandeld door middel van een set cronjobs die een mutatie-tabel aflopen. Deze mutatie-tabel wordt gevuld door twee commands (zie 'php console list' of app/Commands voor de beschikbare commands). Zie ook configuration/jobs.php voor de planning van deze jobs. Let erop dat de mutatierecord-generator voor de elearning **uit** staat. Deze zal dus geen records aanmaken, en niet verwerkt worden. Door 'enabled' => true te zetten kan je deze weer aanzetten.

## Subscribers ##
Op basis van de mutatietabel worden vervolgens de noodzakelijke acties gedaan. Bij bijvoorbeeld mutatie-records van de maintables worden de veranderde gegevens opgehaald, en wordt het abonnee-record toegevoegd of gewijzigd (al naar gelang de noodzaak).

In het geval van de elearning is nu ingesteld dat hier ook de gegevens toegevoegd of gewijzigd worden, én dat in het geval van artikelen en uitgaves een extra set aan acties wordt uitgevoerd met als doel het (her)berekenen van de belangrijke cijfers.

Elke individuele mutatie heeft op basis van {connectie}.{tabel_naam}.{actie} een event-naam, waar een of meerdere Subscribers naar kunnen luisteren. Zie de subscribers onder app/Subscribers voor meer informatie over de acties die gedaan worden.

## Openstaande zaken ##
Er zijn een aantal openstaande zaken met betrekking tot de dataverwerking die nog niet zijn afgehandeld. Ten eerste staat de mutatie-registratie voor de elearning uit (zie hierboven). Wanneer deze aangezet wordt moet dus eerst een import gedaan worden van alle mutaties van de elearning (dit kan met de mutaties-elearning command, zie de opties).

Alle events en subscribers zitten er in, maar de data is nog niet getest op correctheid. Dit moet dus eerst gebeuren voordat dit onderdeel opgenomen kan worden in de ReportingApp.
