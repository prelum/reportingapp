<?php
require __DIR__ . '/vendor/autoload.php';

/**
 * Init applicatie
 */
$app = new \App\Core\App();

/**
 * Voeg de geconfigureerde routes toe
 */
require __DIR__ . '/configuration/routes.php';


/**
 * Boot Laravel Eloquent en Laravel Queue
 *
 * Deze zijn globaal beschikbaar
 */
$capsule = $app->getContainer()->get(\Illuminate\Database\Capsule\Manager::class);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$queue = $app->getContainer()->get(Illuminate\Queue\Capsule\Manager::class);
$queue->setAsGlobal();